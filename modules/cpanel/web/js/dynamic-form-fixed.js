/**
 * yii2-dynamic-form Bug fixed
 *
 * A jQuery plugin to clone form elements in a nested manner, maintaining accessibility.
 *
 * @author Demidenko Andrew <stpatrickw@gmail.com>
 */

    function initSelect2Loading(id, container){
        if (typeof initS2Loading == 'function') {
            initS2Loading(id, container);
        }
    }

    function initSelect2DropStyle(id, kvClose, ev){
        if (typeof initS2Open == 'function') {
            initS2Open(id, kvClose, ev);
        }
    }

