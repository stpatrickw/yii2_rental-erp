<?php

use \yii\widgets\Pjax;

$this->title = \Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Attributes'),
    'url' => \yii\helpers\Url::to(['attribute/list'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<section id="widget-grid" class="">
    <?php $form = \yii\bootstrap\ActiveForm::begin([
        'id' =>  'attribute-form' . $model->id,
        'method' => 'post',
        'options' => [
            'class' => 'form-horizontal',
            'enctype'=>'multipart/form-data'],
        'validateOnChange' => true,
        'validateOnBlur' => true,
        'validateOnType'=> true,
    ]); ?>
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="true" data-widget-editbutton="true">
                <header>
                    <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                    <h2><?= \Yii::t('app', 'Attribute') ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">


                        <?= $form->field($model, 'name',
                                [   'template' => '{label}<div class="col-md-9">{input}</div>{hint}{error}',
                                    'labelOptions' => ['class' => 'col-md-3 control-label'],
                                ])->textInput();

                        ?>


                        <?= $form->field($model, 'attribute_group_id',
                                [   'template' => '{label}<div class="col-md-6">{input}</div>{hint}{error}',
                                    'labelOptions' => ['class' => 'col-md-3 control-label'],
                                ])->dropDownList( \yii\helpers\ArrayHelper::map( \app\models\AttributeGroup::find()->orderBy('name')->asArray()->all(), 'id', 'name' ) , ['prompt' => \Yii::t('app', 'Choose ...')]);
                        ?>

                        <?= $form->field($model, 'sort',
                                [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}',
                                    'labelOptions' => ['class' => 'col-md-3 control-label'],
                                ])->textInput();
                        ?>


                    </div>
                </div>

            </div>

        </article>
    </div>


    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <?= \yii\bootstrap\Html::button(\Yii::t('app','Save'), ['onclick' => 'submitFormButton('.$callback.')', 'class' => 'showModalButton btn btn-success']); ?>

                <?= \yii\helpers\Html::button('<i class="fa fa-angle-left"></i> '. \Yii::t('app','Back'),
                    [
                        'class' => 'btn btn-success pull-left',
                        'data-dismiss' => "modal" ,
                        'aria-hidden' => "true"
                    ]);
                ?>
            </div>
        </div>
    </div>

    <?php
    $save_url = ($model->isNewRecord ? \yii\helpers\Url::to(['attribute/create']) : \yii\helpers\Url::to(['attribute/update', 'id' => $model->id]));

    $this->registerJs(<<<JS

    function submitFormButton(callback){
        var form = $("#attribute-form$model->id"), 
            data = form.data("yiiActiveForm");
            $.each(data.attributes, function() {
                this.status = 3;
            });
            form.yiiActiveForm("validate");
                 
            if(!form.find(".has-error").length) {
                 
                  $.ajax({
                  url: '$save_url',
                  type: 'post',
                  data: form.serialize(),
                  success: function (response) {
                     console.log(response);                  
                    if(!response.error){
                        form.closest('.modal').modal('hide'); 
                        setTimeout(callback, 200);
                    }

                  }
             });    
             }
             
                    
    }


   


JS
        , \yii\web\View::POS_END);

    ?>

    <?php $form->end();?>
</section>

<?php


?>

