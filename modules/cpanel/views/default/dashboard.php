<h2>  Это рабочий стол </h2>
<?php
use app\extensions\smartadmin\widgets\SmartGrid;
use yii\helpers\Html;
use yii\helpers\Url;
use app\extensions\smartadmin\widgets\SmartWidgetSection;
use app\extensions\smartadmin\widgets\SmartWidget;

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;

$this->title = 'Мои последние сметы';

$provider = new ActiveDataProvider([
    'query' => \app\models\Order::find()->estimate(),
    'sort' => false,
]);

?>

<?php SmartWidgetSection::begin(); ?>

<?php
    $columns = [
        [
            'attribute' => 'companyFullName',
            'headerOptions' => ['style'=>'width: 35%'],
            'value' => function($model) {
                if($model->agent)
                    return $model->agent->companyFullName;
                else return '';
            }
        ],
        [
            'attribute' => 'event_name',
            'headerOptions' => ['style'=>'width: 25%'],
        ],
        [
            'attribute' => 'event_start_date',
            'headerOptions' => ['style'=>'width: 15%'],
        ],
        [
            'attribute' => 'event_end_date',
            'headerOptions' => ['style'=>'width: 15%'],
        ],
        [
            'attribute' => 'status_id',
            'headerOptions' => ['style'=>'width: 15%'],
            'format' => 'html',
            'value' => function($model) {
                if($model->status)
                    return $model->status->name;
                else return '';
            }
        ],
        [
            'class'=>'kartik\grid\ActionColumn',
            'headerOptions' => ['style'=>'width: 120px'],
            'width' => '120px',
            'dropdown'=>false,
            'order'=>DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{update} {add_task}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-pencil"></span>', \yii\helpers\Url::to(['estimate/update', 'id' => $model->id]), ['class' => 'btn btn-success', 'data-toggle'=>'tooltip', 'data-placement' => "top", 'title' => "Редактировать"]);
                },
                'add_task' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-arrow-right"></span>', $url, ['class' => 'btn btn-warning', 'data-toggle'=>'tooltip', 'data-placement' => "top", 'title' => "Поставить задачу"]);
                },

            ],
        ]
    ]; ?>

<div class="row">
    <!-- NEW WIDGET START -->
    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">



        <?php
        SmartWidget::begin(['title'=> 'Мои последние сметы', 'icon' => 'fa-table', 'editbutton' => false, 'deletebutton' => false]);

        echo DynaGrid::widget([
            'columns'=>$columns,
            'storage'=>DynaGrid::TYPE_COOKIE,
            'theme'=>'panel-default',
            'gridOptions'=>[
                'dataProvider'=>$provider,
                'tableOptions' => ['style' => 'table-layout: fixed;'],
                //   'filterModel'=>$searchModel,
                'panel'=>['heading'=> false, 'before' => false, 'footer' => false ]
            ],
            'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
        ]);
        SmartWidget::end();

        ?>
    </article>
</div>

        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">



            <?php
            SmartWidget::begin(['icon' => 'fa-table', 'editbutton' => false, 'deletebutton' => false]);

            echo DynaGrid::widget([
                'columns'=>$columns,
                'storage'=>DynaGrid::TYPE_COOKIE,
                'theme'=>'panel-default',
                'gridOptions'=>[
                    'dataProvider'=>$provider,
                    //   'filterModel'=>$searchModel,
                    'panel'=>['heading'=> false, 'before' => false, 'footer' => false ,],
                  //  'tableOptions' => ['style' => 'table-layout: fixed;']
                ],
                'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
            ]);
            SmartWidget::end();

            ?>
            <?php
            SmartWidget::begin(['icon' => 'fa-table', 'editbutton' => false, 'deletebutton' => false]);

            echo DynaGrid::widget([
                'columns'=>$columns,
                'storage'=>DynaGrid::TYPE_COOKIE,
                'theme'=>'panel-default',
                'gridOptions'=>[
                    'dataProvider'=>$provider,
                    //   'filterModel'=>$searchModel,
                    'panel'=>['heading'=> false, 'before' => false, 'footer' => false ,],
                ],
                'options'=>['id'=>'dynagrid-2'] // a unique identifier is important
            ]);
            SmartWidget::end();

            ?>
            <?php
            SmartWidget::begin(['icon' => 'fa-table', 'editbutton' => false, 'deletebutton' => false]);

            echo DynaGrid::widget([
                'columns'=>$columns,
                'storage'=>DynaGrid::TYPE_COOKIE,
                'theme'=>'panel-default',
                'gridOptions'=>[
                    'dataProvider'=>$provider,
                    //   'filterModel'=>$searchModel,
                    'panel'=>['heading'=> false, 'before' => false, 'footer' => false ,],
                ],
                'options'=>['id'=>'dynagrid-3'] // a unique identifier is important
            ]);
            SmartWidget::end();

            ?>


        </article>
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">



            <?php
            SmartWidget::begin(['icon' => 'fa-table', 'editbutton' => false, 'deletebutton' => false]);

            echo DynaGrid::widget([
                'columns'=>$columns,
                'storage'=>DynaGrid::TYPE_COOKIE,
                'theme'=>'panel-default',
                'gridOptions'=>[
                    'dataProvider'=>$provider,
                    //   'filterModel'=>$searchModel,
                    'panel'=>['heading'=> false, 'before' => false, 'footer' => false ,],
                ],
                'options'=>['id'=>'dynagrid-4'] // a unique identifier is important
            ]);
            SmartWidget::end();

            ?>
            <?php
            SmartWidget::begin(['icon' => 'fa-table', 'editbutton' => false, 'deletebutton' => false]);

            echo DynaGrid::widget([
                'columns'=>$columns,
                'storage'=>DynaGrid::TYPE_COOKIE,
                'theme'=>'panel-default',
                'gridOptions'=>[
                    'dataProvider'=>$provider,
                    //   'filterModel'=>$searchModel,
                    'panel'=>['heading'=> false, 'before' => false, 'footer' => false ,],
                ],
                'options'=>['id'=>'dynagrid-5'] // a unique identifier is important
            ]);
            SmartWidget::end();

            ?>
            <?php
            SmartWidget::begin(['icon' => 'fa-table', 'editbutton' => false, 'deletebutton' => false]);

            echo DynaGrid::widget([
                'columns'=>$columns,
                'storage'=>DynaGrid::TYPE_COOKIE,
                'theme'=>'panel-default',
                'gridOptions'=>[
                    'dataProvider'=>$provider,
                    //   'filterModel'=>$searchModel,
                    'panel'=>['heading'=> false, 'before' => false, 'footer' => false ,],

                ],
                'options'=>['id'=>'dynagrid-6'] // a unique identifier is important
            ]);
            SmartWidget::end();

            ?>


        </article>
            
    </div>


<?php SmartWidgetSection::end(); ?>


