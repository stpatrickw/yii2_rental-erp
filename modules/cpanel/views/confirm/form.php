<div class="modal-content">
    <div class="modal-header">
        <div class="bootstrap-dialog-header">
            <div class="bootstrap-dialog-close-button" style="display: block;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <div class="bootstrap-dialog-title" id="w12_title"><?=\Yii::t('app', 'Confirmation') ?></div>
        </div>
    </div>
    <div class="modal-body">
        <div class="bootstrap-dialog-body">
            <div class="bootstrap-dialog-message"><?=\Yii::t('app', 'Are you sure you want to delete ?') ?></div>
        </div>
    </div>
    <div class="modal-footer" style="display: block;">
        <div class="bootstrap-dialog-footer">
            <div class="bootstrap-dialog-footer-buttons">
                <button class="btn btn-default" class="close" data-dismiss="modal" aria-hidden="true" >
                    <span class="glyphicon glyphicon-ban-circle" ></span> <?=\Yii::t('app', 'Cancel') ?>
                </button>
                    <?= \yii\helpers\Html::a('<span class="fa fa-ok"></span> '.\Yii::t('app', 'Delete'), $action_url, ['onclick' => 'submitConfirmButton' . $id . '('.$callback.')', 'class' => 'btn btn-danger']); ?>
            </div>
        </div>
    </div>
</div>


<?php

$this->registerJs(<<<JS

    function submitConfirmButton$id(callback){
    var form = $(this);
        if(form){
            form.closest('.modal').modal('hide');
        } 
        setTimeout(callback, 200);
        
        return false;
        
    }

JS
    , \yii\web\View::POS_END);

?>

