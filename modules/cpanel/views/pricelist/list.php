<?php
use yii\helpers\Html;
use yii\helpers\Url;

use app\extensions\smartadmin\widgets\SmartWidgetSection;
use app\extensions\smartadmin\widgets\SmartWidget;
use kartik\dynagrid\DynaGrid;

$this->title = \Yii::t('app', 'Pricelists');
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

    <div class="row-margin row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?= Html::a('<i class="fa fa-plus"></i> '. \Yii::t('app','Add'), Url::to(['pricelist/create']) , ['class' => 'btn btn-success pull-right']); ?>
        </div>
    </div>

    <?php SmartWidgetSection::begin(); ?>

    <?php
    $columns = [
        [
            'attribute' => 'id',
            'headerOptions' => ['style'=>'width: 7%'],
        ],
        [
            'attribute' => 'pricelist_date',
            'headerOptions' => ['style'=>'width: 15%'],
        ],
        [
            'attribute' => 'description',
            'headerOptions' => ['style'=>'width: 55%'],
        ],
        [
            'attribute' => 'productCount',
            'headerOptions' => ['style'=>'width: 15%'],
        ],
        [
            'attribute' => 'userName',
            'headerOptions' => ['style'=>'width: 15%'],
        ],
        [
            'class'=>'kartik\grid\ActionColumn',
            'dropdown'=>false,
            'headerOptions' => ['style'=>'min-width: 100px'],
            'order'=>DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-pencil"></span>', $url, ['class' => 'btn btn-success']);
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-trash"></span>', $url, ['class' => 'btn btn-danger']);
                },

            ],
        ],
        [
            'class'=>'kartik\grid\CheckboxColumn',  'order'=>DynaGrid::ORDER_FIX_RIGHT,
        ],
    ]; ?>


    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <?php
            SmartWidget::begin(['title' => $this->title, 'icon' => 'fa-table', 'editbutton' => false, 'deletebutton' => false, 'theme' => 'jarviswidget-color-darken']);

            echo DynaGrid::widget([
                'columns'=>$columns,
                'storage'=>DynaGrid::TYPE_COOKIE,
                'theme'=>'panel-default',
                'gridOptions'=>[
                    'dataProvider'=>$provider,
                    //   'filterModel'=>$searchModel,
                    'panel'=>['heading'=> false, 'before' => false],
                ],
                'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
            ]);
            SmartWidget::end();

            ?>

        </article>
    </div>

<?php SmartWidgetSection::end(); ?>
