<?php

use yii\web\JsExpression;
use kartik\select2\Select2;

$this->title = \Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Pricelist'),
    'url' => \yii\helpers\Url::to(['pricelist/list'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<section id="widget-grid" class="">
    <?php $form = \yii\bootstrap\ActiveForm::begin([
        'id' => 'price-form',
        'method' => 'post',
        'options' => [
            'class' => 'form-horizontal',
            'enctype'=>'multipart/form-data'],
        'validateOnChange' => true,
        'validateOnBlur' => true,
        'validateOnType'=> true,
    ]); ?>
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="true" data-widget-editbutton="true">
                <header>
                    <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                    <h2><?= \Yii::t('app', 'Pricelist') ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">


                        <?= $form->field($model, 'id',
                                [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}',
                                    'labelOptions' => ['class' => 'col-md-2 control-label'],
                                ])->textInput(['readonly' => 'true']);
                        ?>

                        <?= $form->field($model, 'pricelist_date',
                            [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}',
                                'labelOptions' => ['class' => 'col-md-2 control-label'],
                            ])->widget( \nex\datepicker\DatePicker::className(), [
                            'addon' => false,
                            'language' => 'ru',
                            'placeholder' => Yii::t('app', 'Choose date...'),
                            'size' => 'sm',
                            'clientOptions' => [
                                'format' => 'YYYY-MM-DD',
                                'stepping' => 30,
                            ],
                        ]);
                        ?>

                        <?= $form->field($model, 'description',
                            [   'template' => '{label}<div class="col-md-10">{input}</div>{hint}{error}',
                                'labelOptions' => ['class' => 'col-md-2 control-label'],
                            ])->textarea();
                        ?>


                        <div class="row row-margin">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <?= \kartik\select2\Select2::widget([
                                    'name' => 'select_products',
                                    'id' => 'select-products',
                                    'data' => \yii\helpers\ArrayHelper::map(\app\models\Product::find()->where(['not in', 'id', $exclude_products])->orderBy('name')->asArray()->all(), 'id', 'name' ),
                                    'language' => 'ru',
                                    'options' => ['placeholder' => \Yii::t('app','Choose the product...')],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'width' => '70%',
                                    ],
                                    'pluginEvents' => [
                                        //  "change" => "function(data) {  }",
                                    ],
                                ]); ?>

                                <?= \yii\bootstrap\Html::a(\Yii::t('app','Add'), '#', ['id'=> 'add-one-product', 'class' => 'btn btn-primary pull-right', 'style' => 'margin-right: 5px']); ?>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <?= \yii\bootstrap\Html::a(\Yii::t('app','Product add all'), '#', ['id'=> 'add-all-products', 'class' => 'btn btn-warning pull-right', 'style' => 'margin-right: 5px']); ?>
                                </div>
                            </div>
                        </div>

                        <?php

                        $columns = [
                            [
                            'attribute' => 'product_id',
                            'headerOptions' => ['style'=>'width: 30%'],
                            'format' => 'raw',
                            'value' => function($model) {
                                //if($model->product)
                                    return ''; //$model->product->name . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][product_id]", $model->product->id ) . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][type]", $model->type );
                            }
                            ]];

                        foreach ($price_types = \app\models\PriceType::find()->orderBy('sort')->all() as $price_type){
                            $columns[] = [
                                'attribute' => 'price_type_id',
                                'label' => $price_type->name . ($price_type->is_dry ? ' (' . \Yii::t('app', 'Dry price') . ')' : ''),
                                'headerOptions' => ['style'=>'width: 5%'],
                                'format' => 'raw',
                                'value' => function($model) {
                                    return ''; //$model->product->name . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][product_id]", $model->product->id ) . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][type]", $model->type );
                                }
                            ];
                        }

                        ?>

                        <table id="product-grid" class="table table-bordered"><thead>
                            <tr>
                                <th style="width: 10%"><?= \Yii::t('app', 'Product/Service') ?></th>
                                <?php foreach ($price_types = \app\models\PriceType::find()->orderBy('sort')->all() as $price_type){
                                    echo '<th style="width: 5%">' .
                                        $price_type->name . ($price_type->is_dry ? ' (' . \Yii::t('app', 'Dry price') . ')' : '')
                                    . '</th>';
                                } ?>
                                <th style="width: 5%"></th>
                            </thead>
                            <tbody>

                                <?php foreach ($listProductPrices as $item) {
                                echo '<tr class="row-item" data-id="'. $item['product']['product_id'] .'">';
                                echo '<td>' . $item['product']['name'] . '</td>';
                                     foreach ($price_types = \app\models\PriceType::find()->orderBy('sort')->all() as $price_type){
                                        echo '<td style="padding: 4px;">';
                                         if(($item['product']['product_type_id'] == 1 && $price_type->for_product) || ($item['product']['product_type_id'] == 2 && $price_type->for_service) ) {
                                           echo '<input style="padding: 0px; text-align: center;" type="text" id="pricelist-id" class="form-control" value="' . (isset($item['prices'][$price_type->id]) ? $item['prices'][$price_type->id] : '' ) . '" name="ProductPrice['. $item['product']['product_id'] .'][' . $price_type->id . ']">';
                                         }
                                        echo '</td>';
                                    }
                                echo '<td><button data-id="' . $item['product']['product_id'] . '" data-name="' . $item['product']['name'] . '" type="button" class="btn btn-danger table-del-button"><span class="fa fa-trash"></span></button></td>';
                                echo '</tr>';
                                }
                                ?>

                            </tbody></table>


                    </div>
                </div>

            </div>

        </article>
    </div>





    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <?= \yii\helpers\Html::submitButton('<i class="fa fa-save"></i> '. \Yii::t('app','Save'),
                    ['class' => 'btn btn-primary']
                ); ?>

                <?= \yii\helpers\Html::a('<i class="fa fa-angle-left"></i> '. \Yii::t('app','Back'), \yii\helpers\Url::to(['estimate/list']) , ['class' => 'btn btn-success pull-left']); ?>
            </div>
        </div>
    </div>

    <?php $form->end();?>
</section>

<?php

$add_one_url = \yii\helpers\Url::to(['pricelist/addone']);
$add_all_url = \yii\helpers\Url::to(['pricelist/addall']);

$this->registerJs(<<<JS

    $(document).ready(function() {   
        $(document).on('click', '.table-del-button', function (e) {
            console.log($(e.currentTarget).data('name'));
            $("#select-products").append($('<option>', {
                    value: $(e.currentTarget).data('id'), 
                    text: $(e.currentTarget).data('name')}));
            $(e.currentTarget).parent().parent().remove();
        });
    });

    $('#add-one-product').click(function(e) {
        product_id = $('#select-products').val();   
    
        if(product_id) {
           
            $.ajax({
                  url: '$add_one_url',
                  type: 'post',
                  data: {add_product_id: product_id},
                  success: function (response) {

                      if(response){
                          html = '<tr class="row-item" data-id="'+product_id+'" '+ (response.product.product_type_id === 2 ? 'style="background-color: beige;"' : '') +'><td>'+ response.product.name +'</td>';
                          $.each(response.price_types, function(index, element) {  
                              html += '<td style="padding: 4px;">';
                              if((response.product.product_type_id === 1 && element.for_product === 1) || (response.product.product_type_id === 2 && element.for_service === 1)){
                                  html += '<input style="'+ (element.product_price ? '' : 'background-color: #fbc6c6;') +' padding: 0px; text-align: center;" type="text" id="pricelist-id" class="form-control" name="ProductPrice['+ response.product.id +'][' + element.id + ']" placeholder="'+ element.product_price +'" >'
                              }
                              html += '</td>';
                          });
                          html += '<td><button data-id="' + product_id + '" data-name="' + response.product.name + '" type="button" class="btn btn-danger table-del-button"><span class="fa fa-trash"></span></button></td>';
                          html += '</tr>';
                           
                          $('#product-grid tbody').append(html);
                          $("#select-products option[value='"+product_id+"']").remove();
                          $("#select-products").val('').change();
                          
                      }

                  }
             }); 
            
            
            
        }
        
        e.stopPropagation();
        e.preventDefault();
    });
    
    
    $('#add-all-products').click(function(e) {
    
        products = [];
        $.each($('.row-item'), function(index, element){
            products.push($(element).data('id'));
        });
        
        console.log(products);
        
            $.ajax({
                  url: '$add_all_url',
                  type: 'post',
                  data: {products: products},
                  success: function (response) {
                      console.log(response);
                      if(response){
                          $.each(response.products, function(pindex, pelement) {
                              html = '<tr class="row-item" data-id="'+pelement.id+'" '+ (pelement.product_type_id === 2 ? 'style="background-color: beige;"' : '') +'><td>'+ pelement.name +'</td>';
                              $.each(response.price_types, function(index, element) {  
                                //  console.log(pelement.prices[element.id]);
                                  html += '<td style="padding: 4px;">';
                                  if((pelement.product_type_id === 1 && element.for_product === 1)|| (pelement.product_type_id === 2 && element.for_service === 1)){
                                      html += '<input style="'+ (pelement.prices[element.id] ? '' : 'background-color: #fbc6c6;') +' padding: 0px; text-align: center;" type="text" id="pricelist-id" class="form-control" name="ProductPrice['+ pelement.id +'][' + element.id + ']" placeholder="'+ pelement.prices[element.id] +'"  >'
                                  }
                                  html += '</td>';
                              });
                              html += '<td><button data-id="' + pelement.id + '" data-name="' + pelement.name + '" type="button" class="btn btn-danger table-del-button"><span class="fa fa-trash"></span></button></td>';
                              html += '</tr>';
                              $('#product-grid tbody').append(html);
                          });
                             
                              $("#select-products").empty();
                      }
                  }

                  
             }); 
            
            
            
                
        e.stopPropagation();
        e.preventDefault();
    });

   
JS
    , \yii\web\View::POS_END);


?>

