<?php

use yii\web\JsExpression;

$this->title = \Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Estimate'),
    'url' => \yii\helpers\Url::to(['estimate/list'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<section id="widget-grid" class="">
    <?php $form = \yii\bootstrap\ActiveForm::begin([
        'id' => 'order-form',
        'method' => 'post',
        'options' => [
            'class' => 'form-horizontal',
            'enctype'=>'multipart/form-data'],
        'validateOnChange' => true,
        'validateOnBlur' => true,
        'validateOnType'=> true,
    ]); ?>
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="true" data-widget-editbutton="true">
                <header>
                    <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                    <h2><?= \Yii::t('app', 'Estimate') ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">


                        <?= $form->field($model, 'id',
                                [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}',
                                    'labelOptions' => ['class' => 'col-md-2 control-label'],
                                    'options' => []
                                ])->textInput(['readonly' => 'true']);
                        ?>

                        <?php $mail_button = \yii\helpers\Html::a('<i class="fa fa-envelope-o"></i> ' . \Yii::t('app', 'Send email')  , '#' , ['class' => 'btn btn-success pull-right', 'style' => 'margin-right: 15px;']); ?>

                        <?= $form->field($model, 'order_date',
                                [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}' . $mail_button,
                                    'labelOptions' => ['class' => 'col-md-2 control-label'],
                                ])->widget( \nex\datepicker\DatePicker::className(), [
                            'addon' => false,
                            'language' => 'ru',
                            'placeholder' => Yii::t('app', 'Choose date...'),
                            'size' => 'sm',
                            'clientOptions' => [
                                'format' => 'YYYY-MM-DD',
                                'stepping' => 30,
                            ],
                        ]);
                        ?>

                        <?= $form->field($model, 'event_name',
                            [   'template' => '{label}<div class="col-md-10">{input}</div>{hint}{error}',
                                'labelOptions' => ['class' => 'col-md-2 control-label'],
                            ])->textInput();
                        ?>

                        <?php $button_create = \yii\bootstrap\Modal::widget([
                            'id' => 'agent-modal',
                            'closeButton' => ['tag' => 'button', 'label' => 'close'],
                            'toggleButton' => [
                                'label' => '<i class="fa fa-plus"></i> ',
                                'tag' => 'a',
                                'data-target' => '#agent-modal',
                                'href' => \yii\helpers\Url::toRoute(['agent/create']),
                                'class' => 'btn btn-success pull-left',
                            ],
                            'clientOptions' => false,
                        ]); ?>

                        <?= $form->field($model, 'agent_id',
                                [   'template' => '{label}<div class="col-md-6">{input}</div>' . $button_create . '{hint}{error}',
                                    'labelOptions' => ['class' => 'col-md-2 control-label'],
                                ])->label(Yii::t('app', 'Customer'))->widget( \kartik\select2\Select2::classname(), [
                            'data' => $listAgents,
                            'language' => 'ru',
                            'options' => ['placeholder' => \Yii::t('app','Choose the customer...')],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            'pluginEvents' => [
                                "change" => "function(data) { fillAgentFields($('#order-agent_id').val()); }",
                            ],
                        ]);
                        ?>

                        <?php
                        $this->registerJs(<<<JS
                            jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
                                 jQuery(".dynamicform_wrapper .dynamic-datepicker").each(function(index) {
                                    $("#productprice-"+index+"-product_price_date").datetimepicker(datetimepicker_b16a0957); 
                                 }); 
                            });
JS
                            , \yii\web\View::POS_END);
                        ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group field-order-event_name">
                                    <label class="col-md-2 control-label" for="agent-fio"><?= Yii::t('app', 'Fio')?></label>
                                    <div class="col-md-2">
                                        <input type="text" id="agent-fio" class="form-control" value="<?= ($model->agent ? $model->agent->fio : '')?>">
                                    </div>
                                    <label class="col-md-1 control-label" for="agent-phone"><?= Yii::t('app', 'Phone')?></label>
                                    <div class="col-md-2">
                                        <input type="text" id="agent-phone" class="form-control" value="<?= ($model->agent ? $model->agent->phone_main : '') ?>">
                                    </div>
                                    <label class="col-md-2 control-label" for="agent-email"><?= Yii::t('app', 'Email')?></label>
                                    <div class="col-md-2">
                                        <input type="text" id="agent-email-test" class="form-control" value="<?= ($model->agent ? $model->agent->email : '') ?>">
                                    </div>

                                </div>
                            </div>
                        </div>

                        <?= $form->field($model, 'event_start_date',
                            [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}',
                                'labelOptions' => ['class' => 'col-md-2 control-label'],
                            ])->widget( \nex\datepicker\DatePicker::className(), [
                            'addon' => false,
                            'language' => 'ru',
                            'placeholder' => Yii::t('app', 'Choose date...'),
                            'value' => 'aaaa',
                            'size' => 'sm',
                            'clientOptions' => [
                                'format' => 'YYYY-MM-DD',
                                'stepping' => 30,
                            ],
                        ]);
                        ?>

                        <?= $form->field($model, 'event_end_date',
                            [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}',
                                'labelOptions' => ['class' => 'col-md-2 control-label'],
                            ])->widget( \nex\datepicker\DatePicker::className(), [
                            'addon' => false,
                            'language' => 'ru',
                            'placeholder' => Yii::t('app', 'Choose date...'),
                            'size' => 'sm',
                            'clientOptions' => [
                                'format' => 'YYYY-MM-DD',
                                'stepping' => 30,
                            ],
                        ]);
                        ?>


                        <?= $form->field($model, 'status_id',
                            [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}',
                                'labelOptions' => ['class' => 'col-md-2 control-label'],
                            ])->dropDownList($statuses);
                        ?>


                        <?php

                        $get_agent_detail_url = \yii\helpers\Url::to(['agent/details']);
                        $this->registerJs(<<<JS
                      
                      function fillAgentFields(id){
                                var data = { agent_id: id};
                                $.ajax({
                                          type: "POST",
                                          url: '$get_agent_detail_url',
                                          dataType: "json",
                                          data: data,
                                          success: function (response) {
                                            console.log(response);
                                            $('#agent-fio').val(response.first_name + ' ' + response.last_name);
                                            if(response.phone_main) {
                                               $('#order-form').find('#agent-phone').val(response.phone_main); 
                                            }else {
                                               $('#order-form').find('#agent-phone').val(''); 
                                            }
                                            console.log(response.phone_main);
                                            console.log(response.email);
                                            
                                            if(response.email !== '') {
                                                console.log('jjjjjj');
                                                $('#order-form #agent-email-test').val(response.email);
                                            } else {
                                                $('#order-form #agent-email-test').val('');
                                            }                                           
                                            
                                          },
                                          error: function (xhr, ajaxOptions, thrownError) {
                                             alert(xhr.responseText);
                                          }
                                        });
                                                                                            
                      }
                      
                      function calculateSum() {
                      
                         var summa = 0; 
                      
                         $("input[field=total_sum]").each( function() {
                            summa = summa + parseFloat($(this).val());
                         })
                           
                         var skidka = parseFloat($('#order-discount').val());  
                           
                         if (!isNaN(skidka)) {
                             summa = summa - skidka;
                         }
                         
                         $('#order-total_sum').val(summa);                               
                        
                               
                        };
                        
                        $("#order-discount").on('keyup paste', calculateSum);
                        
                        
JS
                            , \yii\web\View::POS_END);




                        $product_select = \kartik\select2\Select2::widget([
                            'name' => 'state_1',
                            'id' => 'product-list',
                            'value' => '',
                            'data' => $listProducts,
                            'options' => ['placeholder' => 'Выберите оборудование ...']
                        ]);

                        $product_button_add = \yii\helpers\Html::Button('<i class="fa fa-plus"></i> '. \Yii::t('app','Add'),
                            ['class' => 'btn btn-primary', 'id' => 'product-add-button']
                        );

                        $product_button_add_url = \yii\helpers\Url::to(['product/get-current-price']);

                        $this->registerJs(<<<JS
                       $(document).ready(function() {   
                          $('#product-add-button').click(function() {
                                if($("#product-list :selected").val() && $.isNumeric($("#product-quantity-add").val()) === true && $.isNumeric($("#product-period-add").val()) === true ){
                                
                                data = {product: { 
                                          product_id: $("#product-list :selected").val(),
                                          quantity: $("#product-quantity-add").val(),
                                          period: $("#product-period-add").val() }},
                                $.ajax({
                                          type: "POST",
                                          url: '$product_button_add_url',
                                          dataType: "json",
                                          data: data,
                                          success: function (response) {
                                            console.log(response);
                                            row_id = parseInt($('#tab-widget').attr('data-in-list')) + 1;
                                            $('#tab-widget').attr('data-in-list', row_id); 
                                            $('#wid-table-products table tbody').prepend('' +
                                             '<tr data-key="'+$("#product-list :selected").val()+'">' +
                                                '<td>' + response.name + '<input type="hidden" name="OrderProduct['+row_id+'][product_id]" value="'+ $("#product-list :selected").val() +'"><input type="hidden" name="OrderProduct['+row_id+'][type]" value="1"></td>'
                                              + '<td>' + response.period + '<input type="hidden" name="OrderProduct['+row_id+'][period]" value="'+ response.period +'"></td>'
                                              + '<td>' + response.quantity + '<input type="hidden" name="OrderProduct['+row_id+'][quantity]" value="'+ response.quantity +'"></td>'
                                              + '<td>' + response.price + '<input type="hidden" name="OrderProduct['+row_id+'][price]" value="'+ response.price +'"></td>'
                                              + '<td>' + response.total_sum + '<input type="hidden" name="OrderProduct['+row_id+'][total_sum]" value="'+ response.total_sum +'" field="total_sum"></td>'
                                              + '<td><button type="button" id="table-del-button" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>'
                                              + '</tr>'
                                             );
                                             $("#product-list").select2("val", "");
                                             $("#product-quantity-add").val('');
                                             $("#product-period-add").val('');
                                             
                                             calculateSum();
                                          },
                                          error: function (xhr, ajaxOptions, thrownError) {
                                             alert(xhr.responseText);
                                          }
                                        });
                                                             
                                }else {
                                    alert('Заполните значения !');
                                }
                                
                                }
                            );
                            
                            $(document).on('click', '#table-del-button', function (e) {
                                $(e.currentTarget).parent().parent().remove();
                                calculateSum();
                            } )
                        });
JS
                            , \yii\web\View::POS_END);


                        $first_tab =
                            '<br>
                        <div class="row">
                        <div class="col-sm-12">
                        <div class="form-group field-order-event_name">
                            <label class="col-md-2 control-label" for="order-event_name">Оборудование:</label>
                            <div class="col-md-4">
                                ' . $product_select . '
                            </div>
                            <label class="col-md-1 control-label" for="product-period-add">Период:</label>
                            <div class="col-md-1">
                               <input type="text" id="product-period-add" class="form-control">
                            </div>
                            <label class="col-md-1 control-label" for="product-quantity-add">Кол-во:</label>
                            <div class="col-md-1">
                               <input type="text" id="product-quantity-add" class="form-control">
                            </div>
                            <div class="col-md-1">
                               ' . $product_button_add . '
                            </div>
                        </div>    
                        </div>
                        </div>';
                        $first_tab .= \yii\grid\GridView::widget([
                            'dataProvider' => $dataProductsProvider,
                            'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                            //'caption' => $this->title,
                            'emptyText' => false,
                            'summary' => '',
                            'options' => ['id' => 'wid-table-products',
                                'class' => 'jarviswidget jarviswidget-color-darken',
                                'data-widget-colorbutton' => 'true',
                                'data-product-in-list' => '0',
                                'data-widget-editbutton' => 'true'],
                            'columns' => [
                                [
                                    'attribute' => 'product_id',
                                    'headerOptions' => ['style'=>'width: 30%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                    if($model->product)
                                        return $model->product->name . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][product_id]", $model->product->id ) . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][type]", $model->type );
                                    }
                                ],
                                [
                                    'attribute' => 'period',
                                    'headerOptions' => ['style'=>'width: 15%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->period . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][period]", $model->period );
                                    }
                                ],
                                [
                                    'attribute' => 'quantity',
                                    'headerOptions' => ['style'=>'width: 15%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->quantity . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][quantity]", $model->quantity );
                                    }                                ],
                                [
                                    'attribute' => 'price',
                                    'headerOptions' => ['style'=>'width: 15%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->price . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][price]", $model->price );
                                    }                                ],
                                [
                                    'attribute' => 'total_sum',
                                    'headerOptions' => ['style'=>'width: 15%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->total_sum . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][total_sum]", $model->total_sum, [ 'field' => "total_sum"] );
                                    }                                ],
                                [
                                    'headerOptions' => ['style'=>'min-width: 80px'],
                                    'options' => ['style'=>'min-width: 80px'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return '<button type="button" id="table-del-button" class="btn btn-danger"><i class="fa fa-trash"></i> </button>';
                                    }
                                ]
                            ]

                        ]);



                        $supplier_select = \kartik\select2\Select2::widget([
                            'name' => 'state_3',
                            'id' => 'supplier-list',
                            'value' => '',
                            'data' => $listAgents,
                            'options' => ['placeholder' => Yii::t('app', 'Choose supplier...')]
                        ]);


                        $external_service_select = \kartik\select2\Select2::widget([
                            'name' => 'state_2',
                            'id' => 'external-service-list',
                            'value' => '',
                            'data' => $listServices,
                            'options' => ['placeholder' => 'Выберите услугу...']
                        ]);

                        $external_service_button_add = \yii\helpers\Html::Button('<i class="fa fa-plus"></i> '. \Yii::t('app','Add'),
                            ['class' => 'btn btn-primary', 'id' => 'external-service-add-button']
                        );

                        $this->registerJs(<<<JS
                      $(document).ready(function() {   
                          $('#external-service-add-button').click(function() {
                                if($("#supplier-list :selected").val() && $("#external-service-list").val() 
                                    && $.isNumeric($("#external-service-period-add").val()) === true
                                    && $.isNumeric($("#external-service-quantity-add").val()) === true
                                    && $.isNumeric($("#external-service-price-add").val()) === true
                                    && $.isNumeric($("#external-service-price-supplier-add").val()) === true
                                    && $.isNumeric($("#external-service-vat-add").val()) === true ){ 
                                
                                row_id = parseInt($('#tab-widget').attr('data-in-list')) + 1;
                                            $('#tab-widget').attr('data-in-list', row_id); 
                                            $('#wid-table-external-services table tbody').prepend('' +
                                             '<tr data-key="'+$("#external-service-list :selected").val()+'">' +
                                                '<td>' + $("#supplier-list :selected").text() + '<input type="hidden" name="OrderProduct['+row_id+'][agent_id]" value="'+ $("#supplier-list :selected").val() +'"><input type="hidden" name="OrderProduct['+row_id+'][type]" value="2"></td>'
                                              + '<td>' + $("#external-service-list :selected").text() + '<input type="hidden" name="OrderProduct['+row_id+'][product_id]" value="'+ $("#external-service-list :selected").val() +'"></td>'
                                              + '<td>' + $("#external-service-period-add").val() + '<input type="hidden" name="OrderProduct['+row_id+'][period]" value="'+ $("#external-service-period-add").val() +'"></td>'
                                              + '<td>' + $("#external-service-quantity-add").val() + '<input type="hidden" name="OrderProduct['+row_id+'][quantity]" value="'+ $("#external-service-quantity-add").val() +'"></td>'
                                              + '<td>' + $("#external-service-price-add").val() + '<input type="hidden" name="OrderProduct['+row_id+'][price]" value="'+ $("#external-service-price-add").val() +'"></td>'
                                              + '<td>' + $("#external-service-price-supplier-add").val() + '<input type="hidden" name="OrderProduct['+row_id+'][price_supplier]" value="'+ $("#external-service-price-supplier-add").val() +'"></td>'
                                              + '<td>' + $("#external-service-vat-add").val() + '<input type="hidden" name="OrderProduct['+row_id+'][vat]" value="'+ $("#external-service-vat-add").val() +'"></td>'
                                              + '<td>0</td>'
                                              + '<td>'+ ((parseFloat($("#external-service-price-add").val()) * parseFloat($("#external-service-quantity-add").val())))+'<input type="hidden" name="OrderProduct['+row_id+'][total_sum]" value="' + ((parseFloat($("#external-service-price-add").val()) * parseFloat($("#external-service-quantity-add").val())))+ '"  field="total_sum"></td>'
                                              + '<td><button type="button" id="table-del-button" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>'
                                              + '</tr>'
                                             );
                                             
                                             
                                             $("#supplier-list").select2("val", "");
                                             $("#external-service-list").select2("val", "");
                                             $("#external-service-quantity-add").val('');
                                             $("#external-service-period-add").val('');
                                             $("#external-service-price-supplier-add").val('');
                                             $("#external-service-price-add").val('');
                                             $("#external-service-vat-add").val('');
                                             
                                     calculateSum();
                                              
                                }else {
                                    alert('Заполните значения !');
                                }
                                
                                }
                            );
                            
       
                        });
JS
                            , \yii\web\View::POS_END);

                        $second_tab =
                            '<br>
                        <div class="row">
                        <div class="col-sm-12">
                        <div class="form-group field-order-event_name">
                            <div class="col-md-5">
                                <div class="col-md-12">
                                    <label class="col-md-4 control-label" for="order-event_name">'. \Yii::t('app','Supplier') .'</label>
                                    <div class="col-md-8">
                                        ' . $supplier_select . '
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="col-md-4 control-label" for="order-event_name">'. \Yii::t('app','Service') .'</label>
                                    <div class="col-md-8">
                                        ' . $external_service_select . '
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                              
                                    <div class="col-md-4">
                                        <div class="col-md-12">
                                            <label class="col-md-6 control-label" for="external-service-period-add">Период:</label>
                                            <div class="col-md-6">
                                               <input type="text" id="external-service-period-add" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                        <label class="col-md-6 control-label" for="external-service-quantity-add">Кол-во:</label>
                                        <div class="col-md-6">
                                           <input type="text" id="external-service-quantity-add" class="form-control">
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="col-md-12">
                                        <label class="col-md-8 control-label" for="external-service-price-supplier-add">Цена поставщика</label>
                                        <div class="col-md-4">
                                           <input type="text" id="external-service-price-supplier-add" class="form-control">
                                        </div>
                                        </div>
                                        <div class="col-md-12">
                                        <label class="col-md-8 control-label" for="external-service-price-add">Цена продажи:</label>
                                        <div class="col-md-4">
                                           <input type="text" id="external-service-price-add" class="form-control">
                                        </div>
                                        </div>
                                        <div class="col-md-12">
                                        <label class="col-md-8 control-label" for="external-service-vat-add">НДС</label>
                                        <div class="col-md-4">
                                           <input type="text" id="external-service-vat-add" class="form-control">
                                        </div>
                                        </div>
                                    </div>
                                        <div class="col-md-3">
                                           ' . $external_service_button_add . '
                                         </div>
                                    </div>
              
                        </div>    
                        </div>
                        </div>';
                        $second_tab .= \yii\grid\GridView::widget([
                            'dataProvider' => $dataExternalServiceProvider,
                            'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                            //'caption' => $this->title,
                            'emptyText' => false,
                            'summary' => '',
                            'options' => ['id' => 'wid-table-external-services', 'data-external-services-in-list' => 0],
                            'columns' => [
                                [
                                    'attribute' => 'agent_id',
                                    'headerOptions' => ['style'=>'width: 20%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        if($model->product)
                                            return ($model->agent ? $model->agent->fio : '') . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][product_id]", $model->product->id ) .  \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][type]", $model->type );
                                    }
                                ],                                [
                                    'attribute' => 'product_id',
                                    'label' => Yii::t('app', 'Service'),
                                    'headerOptions' => ['style'=>'width: 20%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        if($model->product)
                                            return $model->product->name . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][product_id]", $model->product->id );
                                    }
                                ],
                                [
                                    'attribute' => 'period',
                                    'headerOptions' => ['style'=>'width: 10%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->period . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][period]", $model->period );
                                    }
                                ],
                                [
                                    'attribute' => 'quantity',
                                    'headerOptions' => ['style'=>'width: 10%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->quantity . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][quantity]", $model->quantity );
                                    }                                ],
                                [
                                    'attribute' => 'price',
                                    'headerOptions' => ['style'=>'width: 10%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->price . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][price]", $model->price );
                                    }
                                ],
                                [
                                    'attribute' => 'price_supplier',
                                    'headerOptions' => ['style'=>'width: 10%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->price_supplier . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][price_supplier]", $model->price_supplier );
                                    }
                                ],
                                [
                                    'attribute' => 'vat',
                                    'headerOptions' => ['style'=>'width: 10%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->price_supplier . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][price_supplier]", $model->price_supplier );
                                    }
                                ],
                                [
                                    'attribute' => 'debt',
                                    'headerOptions' => ['style'=>'width: 10%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return ($model->agent ? $model->agent->debt : '') ;
                                    }
                                ],
                                [
                                    'attribute' => 'total_sum',
                                    'headerOptions' => ['style'=>'width: 20%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->total_sum . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][total_sum]", $model->total_sum, [ 'field' => "total_sum"]  );
                                    }                                ],
                                [
                                    'headerOptions' => ['style'=>'min-width: 80px'],
                                    'options' => ['style'=>'min-width: 80px'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return '<button type="button" id="table-del-button" class="btn btn-danger"><i class="fa fa-trash"></i> </button>';
                                    }
                                ]
                            ]

                        ]);




                        $internal_service_select = \kartik\select2\Select2::widget([
                            'name' => 'state_2',
                            'id' => 'internal-service-list',
                            'value' => '',
                            'data' => $listServices,
                            'options' => ['placeholder' => 'Выберите услугу...']
                        ]);

                        $internal_service_button_add = \yii\helpers\Html::Button('<i class="fa fa-plus"></i> '. \Yii::t('app','Add'),
                            ['class' => 'btn btn-primary', 'id' => 'internal-service-add-button']
                        );

                        $this->registerJs(<<<JS
                      $(document).ready(function() {   
                          $('#internal-service-add-button').click(function() {
                                if( $("#internal-service-list").val() 
                                    && $.isNumeric($("#internal-service-period-add").val()) === true
                                    && $.isNumeric($("#internal-service-quantity-add").val()) === true
                                    && $.isNumeric($("#internal-service-price-add").val()) === true ){
                                
                                row_id = parseInt($('#tab-widget').attr('data-in-list')) + 1;
                                            $('#tab-widget').attr('data-in-list', row_id); 
                                            $('#wid-table-internal-services table tbody').prepend('' +
                                             '<tr data-key="'+$("#internal-service-list :selected").val()+'">' +
                                                '<td>' + $("#internal-service-list :selected").text() + '<input type="hidden" name="OrderProduct['+row_id+'][product_id]" value="'+ $("#internal-service-list :selected").val() +'"><input type="hidden" name="OrderProduct['+row_id+'][type]" value="3"></td>'
                                              + '<td>' + $("#internal-service-period-add").val() + '<input type="hidden" name="OrderProduct['+row_id+'][period]" value="'+ $("#internal-service-period-add").val() +'"></td>'
                                              + '<td>' + $("#internal-service-quantity-add").val() + '<input type="hidden" name="OrderProduct['+row_id+'][quantity]" value="'+ $("#internal-service-quantity-add").val() +'"></td>'
                                              + '<td>' + $("#internal-service-price-add").val() + '<input type="hidden" name="OrderProduct['+row_id+'][price]" value="'+ $("#internal-service-price-add").val() +'"></td>'
                                              + '<td>'+((parseFloat($("#internal-service-price-add").val()) * parseFloat($("#internal-service-quantity-add").val())))+'<input type="hidden" name="OrderProduct['+row_id+'][total_sum]" value="'+((parseFloat($("#internal-service-price-add").val()) * parseFloat($("#internal-service-quantity-add").val())))+'"  field="total_sum"></td>'
                                              + '<td><button type="button" id="table-del-button" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>'
                                              + '</tr>'
                                             );
                                             
                                         
                                             $("#internal-service-list").select2("val", "");
                                             $("#internal-service-quantity-add").val('');
                                             $("#internal-service-period-add").val('');
                                             $("#internal-service-price-add").val('');
                                  
                                             
                                             calculateSum();
                                              
                                }else {
                                    alert('Заполните значения !');
                                }
                                
                                }
                            );
             
                        });
JS
                            , \yii\web\View::POS_END);

                        $third_tab =
                            '<br>
                        <div class="row">
                        <div class="col-sm-12">
                        <div class="form-group field-order-event_name">
                            <div class="col-md-4">
                                    <label class="col-md-4 control-label" for="order-event_name">'. \Yii::t('app','Service') .'</label>
                                    <div class="col-md-8">
                                        ' . $internal_service_select . '
                                    </div>            
                            </div>
                            <div class="col-md-8">
                              
                                    <div class="col-md-3">
                                            <label class="col-md-6 control-label" for="internal-service-period-add">Период:</label>
                                            <div class="col-md-6">
                                               <input type="text" id="internal-service-period-add" class="form-control">
                                            </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="col-md-6 control-label" for="internal-service-quantity-add">Кол-во:</label>
                                        <div class="col-md-6">
                                           <input type="text" id="internal-service-quantity-add" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="col-md-6 control-label" for="internal-service-price-add">Цена:</label>
                                        <div class="col-md-6">
                                           <input type="text" id="internal-service-price-add" class="form-control">
                                        </div>
                                    </div>
                                        <div class="col-md-3">
                                           ' . $internal_service_button_add . '
                                         </div>
                                    </div>
              
                        </div>    
                        </div>
                        </div>';

                        $third_tab .= \yii\grid\GridView::widget([
                            'dataProvider' => $dataInternalServiceProvider,
                            'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                            //'caption' => $this->title,
                            'emptyText' => false,
                            'summary' => '',
                            'options' => ['id' => 'wid-table-internal-services'],
                            'columns' => [
                               [
                                    'attribute' => 'product_id',
                                    'label' => Yii::t('app', 'Service'),
                                    'headerOptions' => ['style'=>'width: 50%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        if($model->product)
                                            return $model->product->name . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][product_id]", $model->product->id ) .  \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][type]", $model->type );
                                    }
                                ],
                                [
                                    'attribute' => 'period',
                                    'headerOptions' => ['style'=>'width: 10%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->period . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][period]", $model->period );
                                    }
                                ],
                                [
                                    'attribute' => 'quantity',
                                    'headerOptions' => ['style'=>'width: 10%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->quantity . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][quantity]", $model->quantity );
                                    }                                ],
                                [
                                    'attribute' => 'price',
                                    'headerOptions' => ['style'=>'width: 10%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->price . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][price]", $model->price );
                                    }
                                ],
                                [
                                    'attribute' => 'total_sum',
                                    'headerOptions' => ['style'=>'width: 10%'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return $model->total_sum . \yii\helpers\Html::input('hidden', "OrderProduct[{$model->id}][total_sum]", $model->total_sum, [ 'field' => "total_sum"]  );
                                    }                                ],
                                [
                                    'headerOptions' => ['style'=>'min-width: 80px'],
                                    'options' => ['style'=>'min-width: 80px'],
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return '<button type="button" id="table-del-button" class="btn btn-danger"><i class="fa fa-trash"></i> </button>';
                                    }
                                ]
                            ]

                        ]);



                        $fourth_tab = '<div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            </div>
                            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">';

                        $fourth_tab .= $form->field($model, 'files')->label(Yii::t('app', 'Files'))->widget(
                            \trntv\filekit\widget\Upload::className(),
                            [
                                'url' => ['file-upload', 'download-file'],
                                'sortable' => true,
                                'maxFileSize' => 10000000, // 10 MiB
                                'maxNumberOfFiles' => 10,
                                'clientOptions' => [
                                    'done' => new JsExpression('function(e, data) { console.log(data);}'),
                                ]
                            ]);
                        $fourth_tab .= '</div>   </div>';



                        echo \yii\bootstrap\Tabs::widget([
                        'options' => ['id' => 'tab-widget','data-in-list' => $model->maxOrderProductId],
                        'items' => [
                            [
                            'label' => \Yii::t('app', 'Equipment'),
                            'content' => $first_tab,
                            'active' => true
                            ],
                            [
                            'label' => \Yii::t('app', 'External services'),
                            'content' => $second_tab,
                         //   'active' => true,
                            //'headerOptions' => [],
                            ],
                            [
                            'label' => \Yii::t('app', 'Internal services'),
                            'content' => $third_tab,
                           // 'active' => true,
                            //'headerOptions' => [],
                            ],
                            [
                            'label' => \Yii::t('app', 'Files'),
                            'content' => $fourth_tab,
                           // 'active' => true,
                            //'headerOptions' => [],
                            ],
                        ],
                        ]);

                        ?>


                        <?= $form->field($model, 'discount',
                                [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}',
                                    'labelOptions' => ['class' => 'col-md-2 control-label'],
                                ])->textInput();
                        ?>

                        <?= $form->field($model, 'total_sum',
                                [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}',
                                    'labelOptions' => [ 'class' => 'col-md-2 control-label',
                                                        'style' => 'font-weight: bold;'],
                                ])->textInput(['readonly' => 'true']);
                        ?>

                    </div>
                </div>

            </div>

        </article>
    </div>


    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <?= \yii\helpers\Html::submitButton('<i class="fa fa-save"></i> '. \Yii::t('app','Save'),
                    ['class' => 'btn btn-primary']
                ); ?>

                <?= \yii\helpers\Html::a('<i class="fa fa-angle-left"></i> '. \Yii::t('app','Back'), \yii\helpers\Url::to(['estimate/list']) , ['class' => 'btn btn-success pull-left']); ?>
            </div>
        </div>
    </div>

    <?php $form->end();?>
</section>

<?php


?>

