<?php

use wbraganca\dynamicform\DynamicFormWidget;

DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
    'widgetBody' => '.container-items', // required: css class selector
    'widgetItem' => '.item', // required: css class
    'limit' => 10, // the maximum times, an element can be cloned (default 999)
    'min' => 0, // 0 or 1 (default 1)
    'insertButton' => '.add-item', // css class
    'deleteButton' => '.remove-item-attribute', // css class
    'model' => $modelsAttribute[0],
    'formId' => 'product-form' . $model->id,
    'formFields' => [
        'value',

    ],
]);
?>

<div class="container-items">

<?php
    foreach ($modelsAttribute as $index => $modelAttribute) { ?>

    <div class='item'>
    <div class='row row-margin'>
    <div class="col-xs-10 col-sm-10 col-md-11 col-lg-11">

    <?= $form->field($modelAttribute, "[{$index}]attribute_id",
        [   'template' => '{label}<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">{input}</div>' . '{hint}{error}',
            'labelOptions' => ['class' => 'col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label'],
            'options' => ['class' => 'col-md-6', 'style' => 'text-align: left; ']
        ])->label(Yii::t('app', 'Attribute'))->widget( \kartik\select2\Select2::classname(), [
        'data' => \app\models\Attribute::getAttributesArrayForSelect(),
        'language' => 'ru',
        'options' => ['placeholder' => \Yii::t('app','Choose the attribute...')],
        'pluginOptions' => [
            'allowClear' => true
        ],
        'pluginEvents' => [
            //  "change" => "function(data) {  }",
        ],
    ]); ?>

    <?= $form->field($modelAttribute, "[{$index}]value",
        [   'template' => '{label}<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">{input}</div>{hint}{error}',
            'labelOptions' => ['class' => 'col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label',
            ],
            'options' => ['class' => 'col-md-6']
        ])->textInput(['placeholder' => $modelAttribute->getAttributeLabel( 'value' )]);
    ?>

    <?php   if (! $modelAttribute->isNewRecord) {
        echo \yii\helpers\Html::activeHiddenInput($modelAttribute, "[{$index}]id");
    }
    ?>

    </div>
    <div class="col-xs-2 col-sm-2 col-md-1 col-lg-1">
        <button type="button" data-toggle="tooltip" title="" class="btn btn-danger pull-right remove-item-attribute" data-original-title="Remove">
            <i class="fa fa-trash"></i>
        </button>
    </div>
    </div>
    </div>
    
<?php } ?>

</div>
<div class="row" style="padding-top: 10px; border-top: solid #bbbbbb 1px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <button type="button" data-toggle="tooltip" title="" class="btn btn-primary pull-right add-item" data-original-title="Add"><i class="fa fa-plus-circle"></i></button>
    </div>
</div>

<?php DynamicFormWidget::end(); ?>

