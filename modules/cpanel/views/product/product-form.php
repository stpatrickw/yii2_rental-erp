<?php

use \yii\widgets\Pjax;
use wbraganca\dynamicform\DynamicFormWidget;

$this->title = \Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Products and services'),
    'url' => \yii\helpers\Url::to(['product/list'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<section id="widget-grid" class="">
    <?php $form = \yii\widgets\ActiveForm::begin([
        'id' =>  'product-form' . $model->id,
        'method' => 'post',
        'options' => [
            'class' => 'form-horizontal',
            'enctype'=>'multipart/form-data'],
        'validateOnChange' => true,
        'validateOnBlur' => true,
        'validateOnType'=> true,
    ]); ?>
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="true" data-widget-editbutton="true">
                <header>
                    <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                    <h2><?= \Yii::t('app', 'Product') ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">


                        <?= $form->field($model, 'name',
                            [   'template' => '{label}<div class="col-md-10">{input}</div>{hint}{error}',
                                'labelOptions' => ['class' => 'col-md-2 control-label'],
                            ])->textInput();
                        ?>

                        <?= $form->field($model, 'category_id',
                            [   'template' => '{label}<div class="col-md-10">{input}</div>{hint}{error}',
                                'labelOptions' => ['class' => 'col-md-2 control-label'],
                            ])->dropDownList($listCategories);
                        ?>

                        <?= $form->field($model, 'model',
                            [   'template' => '{label}<div class="col-md-10">{input}</div>{hint}{error}',
                                'labelOptions' => ['class' => 'col-md-2 control-label'],
                            ])->textInput();
                        ?>

                        <?= $form->field($model, 'barcode',
                            [   'template' => '{label}<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">{input}</div><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>{hint}{error}',
                                'labelOptions' => ['class' => 'col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label'],
                            ])->textInput(['value' => isset($barcode) ? $barcode : $model->barcode]);
                        ?>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <?= $form->field($model, 'image',
                                    [   'template' => '{label}<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">{input}</div><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>{hint}{error}',
                                        'labelOptions' => ['class' => 'col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label']]
                                )->label(Yii::t('app', 'Image'))->widget(
                                    \trntv\filekit\widget\Upload::className(),
                                    [
                                        'id' => 'wid-up'.$model->id,
                                        'url' => ['image-upload'],
                                    ]);
                                ?>
                            </div>
                        </div>


                        <?= $form->field($model, 'product_type_id')->hiddenInput(['value' => \app\models\Product::TYPE_PRODUCT])->label(false);
                        ?>

                        <?= \yii\bootstrap\Tabs::widget([
                            'options' => ['id' => 'tab-widget' . $model->id, 'style' => 'margin-bottom: 15px'],
                            'items' => [
                                [
                                    'label' => \Yii::t('app', 'Attributes'),
                                    'content' => $this->render('product-form-tab-attribute', ['form' => $form, 'model' => $model, 'modelsAttribute' => $modelsAttribute]),
                                    'active' => true
                                ],
                                [
                                    'label' => \Yii::t('app', 'Variants'),
                                    'content' => $this->render('product-form-tab-variant', ['form' => $form, 'model' => $model, 'modelsVariant' => $modelsVariant]),
                                    //  'active' => true,
                                    //'headerOptions' => [],
                                ],
                                [
                                    'label' => \Yii::t('app', 'Prices'),
                                    'content' => '',
                                    //   'active' => true,
                                    //'headerOptions' => [],
                                ],

                            ],
                        ]); ?>

                    </div>
                </div>

            </div>

        </article>
    </div>


    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <?= \yii\bootstrap\Html::button(\Yii::t('app','Save'), ['onclick' => 'submitFormButton('.$callback.')', 'class' => 'showModalButton btn btn-success']); ?>

                <?= \yii\helpers\Html::button('<i class="fa fa-angle-left"></i> '. \Yii::t('app','Back'),
                    [
                        'class' => 'btn btn-success pull-left',
                        'data-dismiss' => "modal" ,
                        'aria-hidden' => "true"
                    ]);
                ?>
            </div>
        </div>
    </div>

    <?php
    $save_url = ($model->isNewRecord ? \yii\helpers\Url::to(['product/product-create']) : \yii\helpers\Url::to(['product/update', 'id' => $model->id]));

    $this->registerJs(<<<JS

    function submitFormButton(callback){
        var form = $("#product-form$model->id"), 
            data = form.data("yiiActiveForm");
            $.each(data.attributes, function() {
                this.status = 3;
            });
            form.yiiActiveForm("validate");
                 
            if(!form.find(".has-error").length) {
                 
                  $.ajax({
                  url: '$save_url',
                  type: 'post',
                  data: form.serialize(),
                  success: function (response) {
                     console.log(response);                  
                    if(!response.error){
                        form.closest('.modal').modal('hide'); 
                        setTimeout(callback, 200);
                    }

                  }
             });    
             }
             
                    
    }


   


JS
        , \yii\web\View::POS_END);

    ?>

    <?php $form->end();?>
</section>



