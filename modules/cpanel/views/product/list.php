<?php
use app\extensions\smartadmin\widgets\SmartGrid;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use app\components\widgets\Modal;
use app\extensions\smartadmin\widgets\SmartWidgetSection;
use app\extensions\smartadmin\widgets\SmartWidget;

$this->title = \Yii::t('app', 'Product');
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>


    <div class="row-margin row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <?= Modal::widget([
                'id' => 'service-modal-create',
                'closeButton' => ['tag' => 'button', 'label' => 'close'],
                'toggleButton' => [
                    'label' => '<i class="fa fa-plus"></i> ' .\Yii::t('app','Add service'),
                    'tag' => 'a',
                    'data-target' => '#service-modal-create',
                    'href' => \yii\helpers\Url::toRoute(['product/service-create', 'callback' => 'buttonCallback']),
                    'class' => 'btn btn-success pull-right',
                ],
                'clientOptions' => false,
            ]); ?>

            <?= Modal::widget([
                'id' => 'product-modal-create',
                'closeButton' => ['tag' => 'button', 'label' => 'close'],
                'options' => ['tabindex' => false],
                'size' => Modal::SIZE_LARGE,
                'toggleButton' => [
                    'label' => '<i class="fa fa-plus"></i> ' . \Yii::t('app','Add product'),
                    'tag' => 'a',
                    'data-target' => '#product-modal-create',
                    'href' => \yii\helpers\Url::toRoute(['product/product-create', 'callback' => 'buttonCallback']),
                    'class' => 'btn btn-success pull-right',
                    'style' => 'margin-right: 10px'
                ],
                'clientOptions' => false,
            ]); ?>

        </div>
    </div>

<?php SmartWidgetSection::begin(); ?>
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <?php
            $columns = [
                [
                    'attribute' => 'name',
                    'headerOptions' => ['style'=>'width: 55%'],
                ],
                [
                    'attribute' => 'categoryName',
                    'headerOptions' => ['style'=>'width: 30%'],
                ],
                [
                    'attribute' => 'product_type_id',
                    'headerOptions' => ['style'=>'width: 10%'],
                    'format' => 'html',
                    'value'=>function ($model) {
                        switch($model->product_type_id){
                            case \app\models\Product::TYPE_PRODUCT: return \Yii::t('app', 'Product');
                            case \app\models\Product::TYPE_SERVICE: return \Yii::t('app', 'Service');
                            default: return '';
                        }

                    },
                ],
                [
                    'class'=>'kartik\grid\ActionColumn',
                    'dropdown'=>false,
                    'headerOptions' => ['style'=>'min-width: 100px'],
                    'order'=>DynaGrid::ORDER_FIX_RIGHT,
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {

                            $button_update = Modal::widget([
                                'id' => 'product-modal-update'.$model->id,
                                'closeButton' => ['tag' => 'button', 'label' => 'close'],
                                'options' => ['tabindex' => false],
                                'size' => ($model->product_type_id == \app\models\Product::TYPE_PRODUCT ? Modal::SIZE_LARGE : Modal::SIZE_DEFAULT),
                                'toggleButton' => [
                                    'label' => '<i class="fa fa-pencil"></i> ',
                                    'tag' => 'a',
                                    'data-target' => '#product-modal-update'.$model->id,
                                    'href' => \yii\helpers\Url::toRoute(['product/update', 'id' => $model->id, 'callback' => 'buttonCallback']),
                                    'class' => 'btn btn-success pull-left',
                                ],
                                'clientOptions' => false,
                            ]);
                            return $button_update;
                        },
                        'delete' => function ($url, $model, $key) {
                            $button_delete = Modal::widget([
                                'id' => 'product-modal-delete' . $model->id,
                                'options' => ['class' => 'bootstrap-dialog type-danger fade'],
                                'closeButton' => ['tag' => 'button', 'label' => 'x'],
                                'toggleButton' => [
                                    'label' => '<i class="fa fa-trash"></i> ',
                                    'tag' => 'a',
                                    'data-target' => '#product-modal-delete'.$model->id,
                                    'href' => \yii\helpers\Url::toRoute(['confirm/index', 'model' => 'product', 'id' => $model->id, 'action' => 'delete']),
                                    'class' => 'btn btn-danger',
                                ],
                                'clientOptions' => false,
                            ]);
                            return $button_delete;
                        },

                    ],
                ],
                [
                    'class'=>'kartik\grid\CheckboxColumn',  'order'=>DynaGrid::ORDER_FIX_RIGHT,
                ],
            ]; ?>

            <?php

            $onSelect = new \yii\web\JsExpression(<<<JS
    function (undefined, item) {
    console.log(item);
    $.pjax.reload({
        container:"#pjax-products",
        type: 'GET',
        url: item.href
    });
}
JS
            );
            $tree = \app\components\widgets\TreeView::widget([
                'data' => \app\models\CategoryProduct::getChidsArray(),
                'size' => \execut\widget\TreeView::SIZE_MIDDLE,
                'search' => false,
                'header' => \Yii::t('app','Categories'),
                'searchOptions' => [
                  //  'inputOptions' => [
                  //      'placeholder' => 'Search category...'
                  //  ],
                ],
                'clientOptions' => [
                    'onNodeSelected' => $onSelect,
                   // 'selectedBackColor' => 'rgb(40, 153, 57)',
                    'borderColor' => '#ddd',
                    'enableLinks' => false
                ],
            ]);



            SmartWidget::begin(['title' => $this->title, 'icon' => 'fa-table', 'editbutton' => false, 'deletebutton' => false, 'theme' => 'jarviswidget-color-darken']);
            ?>


           <div class="col-sm-4 col-md-4 col-lg-3">
                <?= $tree ?>
                <div class="row-margin row">
                    <a href="<?= Url::to(['product/list'])?>" class="btn btn-primary pull-left"?><?= \Yii::t('app', 'Show all') ?></a>
                </div>
            </div>

            <div class="col-sm-8 col-md-8 col-lg-9">

            <?php Pjax::begin(['id' => 'pjax-products']) ?>
            <?= DynaGrid::widget([
                'columns'=>$columns,
                'storage'=>DynaGrid::TYPE_COOKIE,
                'theme'=>'panel-default',
                'gridOptions'=>[
                    'dataProvider'=>$provider,
                    //   'filterModel'=>$searchModel,
                    'panel'=>['heading'=> false, 'before' => false],
                ],
                'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
            ]); ?>
            <?php Pjax::end(); ?>
            </div>

            <?php SmartWidget::end(); ?>



        </article>
    </div>
<?php SmartWidgetSection::end(); ?>


<?php

$this->registerJs(<<<JS

    function buttonCallback(form){
        $.pjax.reload({container:"#pjax-products"});  //Reload GridView    
    }  


JS
    , \yii\web\View::POS_END);

?>