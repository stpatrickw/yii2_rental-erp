<?php

use wbraganca\dynamicform\DynamicFormWidget;

DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper2', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
    'widgetBody' => '.container-items2', // required: css class selector
    'widgetItem' => '.item2', // required: css class
    'limit' => 10, // the maximum times, an element can be cloned (default 999)
    'min' => 0, // 0 or 1 (default 1)
    'insertButton' => '.add-item2', // css class
    'deleteButton' => '.remove-item2', // css class
    'model' => $modelsVariant[0],
    'formId' => 'product-form' . $model->id,
    'formFields' => [
        'value',

    ],
]);
?>

<div class="container-items2">

<?php
    foreach ($modelsVariant as $index => $modelVariant) { ?>

    <div class='item2'>
    <div class='row row-margin'>
    <div class="col-xs-9 col-sm-9 col-md-10 col-lg-10">

    <?= $form->field($modelVariant, "[{$index}]barcode",
        [   'template' => '{label}<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">{input}</div>{hint}{error}',
            'labelOptions' => ['class' => 'col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label',
            ],
            'options' => ['class' => 'col-md-6']
        ])->textInput(['placeholder' => $modelVariant->getAttributeLabel( 'barcode' )]);
    ?>

    <?= $form->field($modelVariant, "[{$index}]serial",
        [   'template' => '{label}<div class="col-xs-8 col-sm-8 col-md-7 col-lg-7">{input}</div>{hint}{error}',
            'labelOptions' => ['class' => 'col-xs-4 col-sm-4 col-md-5 col-lg-5 control-label',
            ],
            'options' => ['class' => 'col-md-6']
        ])->textInput(['placeholder' => $modelVariant->getAttributeLabel( 'serial' )]);
    ?>

     <?php   if (! $modelVariant->isNewRecord) {
        echo \yii\helpers\Html::activeHiddenInput($modelVariant, "[{$index}]id");
        }
     ?>

    </div>
    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
        <button type="button" data-toggle="tooltip" data-placement="top" title="<?= \Yii::t('app', 'Delete') ?>"  class="btn btn-danger pull-right remove-item2" data-original-title="Remove">
            <i class="fa fa-trash"></i>
        </button>
        <button type="button" data-toggle="tooltip" data-placement="top" title="<?= \Yii::t('app', 'History') ?>" style="margin-right: 5px;" class="btn btn-warning pull-right" data-original-title="History">
            <i class="fa fa-history"></i>
        </button>
        <button type="button" data-toggle="tooltip" data-placement="top" title="<?= \Yii::t('app', 'Print') ?>" style="margin-right: 5px;" class="btn btn-success pull-right" data-original-title="Print barcode">
            <i class="fa fa-print"></i>
        </button>
    </div>
    </div>
    </div>
    
<?php } ?>

</div>
<div class="row" style="padding-top: 10px; border-top: solid #bbbbbb 1px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <button type="button" data-toggle="tooltip" title="" class="btn btn-primary pull-right add-item2" data-original-title="Add"><i class="fa fa-plus-circle"></i></button>
    </div>
</div>

<?php DynamicFormWidget::end(); ?>

