<?php
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use app\extensions\smartadmin\widgets\SmartWidgetSection;
use app\extensions\smartadmin\widgets\SmartWidget;
use yii\widgets\Pjax;

$this->title = \Yii::t('app', 'Attribute groups');
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="row-margin row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php echo $button_create = \yii\bootstrap\Modal::widget([
            'id' => 'attr-group-modal-create',
            'closeButton' => ['tag' => 'button', 'label' => 'close'],
            'toggleButton' => [
                'label' => '<i class="fa fa-plus"></i> ' . \Yii::t('app','Add'),
                'tag' => 'a',
                'data-target' => '#attr-group-modal-create',
                'href' => \yii\helpers\Url::toRoute(['attribute-group/create', 'callback' => 'buttonCallback']),
                'class' => 'btn btn-success pull-right',
            ],
            'clientOptions' => false,
        ]); ?>
    </div>
</div>

<?php SmartWidgetSection::begin(); ?>


<?php
$columns = [
    [
        'attribute' => 'name',
        'headerOptions' => ['style'=>'width: 88%'],
    ],
    [
        'class'=>'kartik\grid\ActionColumn',
        'dropdown'=>false,
        'headerOptions' => ['style'=>'min-width: 100px'],
        'order'=>DynaGrid::ORDER_FIX_RIGHT,
        'template' => '{update} {delete}',
        'buttons' => [
            'update' => function ($url, $model, $key) {

                $button_update = \yii\bootstrap\Modal::widget([
                    'id' => 'attr-group-modal-update'.$model->id,
                    'closeButton' => ['tag' => 'button', 'label' => 'close'],
                    'toggleButton' => [
                        'label' => '<i class="fa fa-pencil"></i> ',
                        'tag' => 'a',
                        'data-target' => '#attr-group-modal-update'.$model->id,
                        'href' => \yii\helpers\Url::toRoute(['attribute-group/update', 'id' => $model->id, 'callback' => 'buttonCallback']),
                        'class' => 'btn btn-success pull-left',
                    ],
                    'clientOptions' => false,
                ]);
                return $button_update;
                //  return Html::a('<span class="fa fa-pencil"></span>', $url, ['class' => 'btn btn-success']);
            },
            'delete' => function ($url, $model, $key) {
                $button_delete = \yii\bootstrap\Modal::widget([
                    'id' => 'attr-group-modal-delete' . $model->id,
                    'options' => ['class' => 'bootstrap-dialog type-danger fade'],
                    'closeButton' => ['tag' => 'button', 'label' => 'x'],
                    'toggleButton' => [
                        'label' => '<i class="fa fa-trash"></i> ',
                        'tag' => 'a',
                        'data-target' => '#attr-group-modal-delete'.$model->id,
                        'href' => \yii\helpers\Url::toRoute(['confirm/index', 'model' => 'attribute-group', 'id' => $model->id, 'action' => 'delete']),
                        'class' => 'btn btn-danger',
                    ],
                    'clientOptions' => false,
                ]);
                return $button_delete;
            },

        ],
    ],
    [
        'class'=>'kartik\grid\CheckboxColumn',  'order'=>DynaGrid::ORDER_FIX_RIGHT,
    ],
]; ?>


<div class="row">
    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <?php
        SmartWidget::begin(['title' => $this->title, 'icon' => 'fa-table', 'editbutton' => false, 'deletebutton' => false, 'theme' => 'jarviswidget-color-darken']);
        ?>

        <?php Pjax::begin(['id' => 'pjax-attr-group' ]) ?>

        <?php echo DynaGrid::widget([
            'columns'=>$columns,
            'storage'=>DynaGrid::TYPE_COOKIE,
            'theme'=>'panel-default',
            'gridOptions'=>[
                'dataProvider'=> $provider,
             //   'filterModel'=> $searchModel,
                'panel'=>['heading'=> false, 'before' => false],
            ],
            'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
        ]); ?>

        <?php Pjax::end() ?>
        <?php SmartWidget::end(); ?>



    </article>
</div>
<?php SmartWidgetSection::end(); ?>

<?php

$this->registerJs(<<<JS

    function buttonCallback(form){
        $.pjax.reload({container:"#pjax-attr-group"});  //Reload GridView    
    }  


JS
    , \yii\web\View::POS_END);

?>
