<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it -->

					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<img src="/img/avatars/sunny.png" alt="me" class="online">
						<span>
							Admin
						</span>
						<i class="fa fa-angle-down"></i>
					</a>

				</span>
    </div>
    <!-- end user info -->

    <nav>

        <ul style="">
            <li <?php if(Yii::$app->controller->id == "default") echo "class='active'" ?>>
                <?php $views_count = 5; ?>
                <a href="/" title="<?= \Yii::t('app','Dashboard')?>"><i class="fa fa-lg fa-fw fa-desktop"></i> <?php if($views_count) { ?> <span class="badge pull-right inbox-badge margin-right-13 bg-color-teal"><?= $views_count ?></span> <?php } ?> <span class="menu-item-parent"><?= \Yii::t('app','Dashboard')?></span></a>
            </li>
            <li <?php if(Yii::$app->controller->id == "company") echo "class='active'" ?>>
                <a href="<?= \yii\helpers\Url::to(['company/list']) ?>" title="<?= \Yii::t('app','Companies')?>"><i class="fa fa-lg fa-fw fa-university"></i> <span class="menu-item-parent"><?= \Yii::t('app','Companies')?></span></a>
            </li>
            <li <?php if(Yii::$app->controller->id == "agents") echo "class='active'" ?>>
                <a href="<?= \yii\helpers\Url::to(['agent/list']) ?>" title="<?= \Yii::t('app','Agents')?>"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent"><?= \Yii::t('app','Agents')?></span></a>
            </li>
            <li <?php if(Yii::$app->controller->id == "warehouse") echo "class='active'" ?>>
                <a href="<?= \yii\helpers\Url::to(['warehouse/list']) ?>"><i class="fa fa-lg fa-fw fa-cubes"></i> <span class="menu-item-parent"><?= \Yii::t('app', 'Warehouses'); ?></span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-lg fa-fw fa-navicon"></i> <span class="menu-item-parent"><?= \Yii::t('app', 'Nomenclature'); ?></span></a>
                <ul>
                    <li <?php if(Yii::$app->controller->id == "category-product") echo "class='active'" ?>>
                        <a href="<?= \yii\helpers\Url::to(['category-product/list']) ?>"><i class="fa fa-lg fa-fw fa-folder"></i> <span class="menu-item-parent"><?= \Yii::t('app', 'Product categories'); ?></span></a>
                    </li>
                    <li <?php if(Yii::$app->controller->id == "product" && (Yii::$app->controller->action->id == "list") ) echo "class='active'" ?>>
                        <a href="<?= \yii\helpers\Url::to(['product/list']) ?>"><i class="fa fa-lg fa-fw fa-shopping-cart"></i> <span class="menu-item-parent"><?= \Yii::t('app', 'Products and services'); ?></span></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-fw fa-check-square-o"></i> <span class="menu-item-parent"><?= \Yii::t('app', 'Attributes'); ?></span></a>
                        <ul>
                            <li <?php if(Yii::$app->controller->id == "attribute-group") echo "class='active'" ?>>
                                <a href="<?= \yii\helpers\Url::to(['attribute-group/list']) ?>"> <span class="menu-item-parent"><?= \Yii::t('app', 'Groups'); ?></span></a>
                            </li>
                            <li <?php if(Yii::$app->controller->id == "attribute") echo "class='active'" ?>>
                                <a href="<?= \yii\helpers\Url::to(['attribute/list']) ?>"> <span class="menu-item-parent"><?= \Yii::t('app', 'Attributes'); ?></span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-lg fa-fw fa-tags"></i> <span class="menu-item-parent"><?= \Yii::t('app', 'Pricing'); ?></span></a>
                <ul>
                    <li <?php if(Yii::$app->controller->id == "pricelist" && (Yii::$app->controller->action->id == "list" || Yii::$app->controller->action->id == "create" || Yii::$app->controller->action->id == "update")) echo "class='active'" ?>>
                        <a href="<?= \yii\helpers\Url::to(['pricelist/list']) ?>" title="<?= \Yii::t('app','Price lists')?>"><i class="fa fa-lg fa-fw fa-tags"></i> <span class="menu-item-parent"><?= \Yii::t('app','Price lists')?></span></a>
                    </li>
                    <li <?php if(Yii::$app->controller->id == "price-type") echo "class='active'" ?>>
                        <a href="<?= \yii\helpers\Url::to(['price-type/list']) ?>"><i class="fa fa-lg fa-fw fa-sliders"></i> <span class="menu-item-parent"><?= \Yii::t('app', 'Price types'); ?></span></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-lg fa-fw fa-list-alt"></i> <span class="menu-item-parent"><?= \Yii::t('app', 'Documents'); ?></span></a>
                <ul>
                    <li <?php if(Yii::$app->controller->id == "doc-receive") echo "class='active'" ?>>
                        <a href="<?= \yii\helpers\Url::to(['doc-receive/list']) ?>" title="<?= \Yii::t('app','Doc receive product')?>"><i class="fa fa-lg fa-fw fa-calculator txt-color-orange"></i> <span class="menu-item-parent"><?= \Yii::t('app','Doc receive product')?> (В разработке)</span></a>
                    </li>
                    <li <?php if(Yii::$app->controller->id == "doc-estimate") echo "class='active'" ?>>
                        <a href="<?= \yii\helpers\Url::to(['doc-estimate/list']) ?>" title="<?= \Yii::t('app','My estimates')?>"><i class="fa fa-lg fa-fw fa-calculator txt-color-orange"></i> <span class="menu-item-parent"><?= \Yii::t('app','Estimates')?></span></a>
                    </li>
                </ul>
            </li>            <li>
                <a href="#"><i class="fa fa-lg fa-fw fa-cogs"></i> <span class="menu-item-parent"><?= \Yii::t('app', 'Settings'); ?></span><b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b></a>
                <ul>
                    <li <?php if(Yii::$app->controller->id == "user") echo "class='active'" ?>>
                        <a href="<?= \yii\helpers\Url::to(['user/list']) ?>"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent"><?= \Yii::t('app', 'Users'); ?></span></a>
                    </li>
                    <li <?php if(Yii::$app->controller->id == "status") echo "class='active'" ?>>
                        <a href="<?= \yii\helpers\Url::to(['status/list']) ?>"><i class="fa fa-lg fa-fw fa-check-circle"></i> <span class="menu-item-parent"><?= \Yii::t('app', 'Statuses'); ?></span></a>
                    </li>

                </ul>
            </li>

        </ul>
    </nav>


			<span class="minifyme" data-action="minifyMenu">
				<i class="fa fa-arrow-circle-left hit"></i>
			</span>

</aside>