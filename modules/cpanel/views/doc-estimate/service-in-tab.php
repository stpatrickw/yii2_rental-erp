<?php

$internal_service_select = \kartik\select2\Select2::widget([
    'name' => 'state_2',
    'id' => 'internal-service-list',
    'value' => '',
    'data' => $listServices,
    'options' => ['placeholder' => 'Выберите услугу...']
]);

$internal_service_button_add = \yii\helpers\Html::Button('<i class="fa fa-plus"></i> '. \Yii::t('app','Add'),
    ['class' => 'btn btn-primary', 'id' => 'internal-service-add-button']
);

$this->registerJs(<<<JS
                      $(document).ready(function() {   
                          $('#internal-service-add-button').click(function() {
                                if( $("#internal-service-list").val() 
                                    && $.isNumeric($("#internal-service-period-add").val()) === true
                                    && $.isNumeric($("#internal-service-quantity-add").val()) === true
                                    && $.isNumeric($("#internal-service-price-add").val()) === true ){
                                
                                row_id = parseInt($('#tab-widget').attr('data-in-list')) + 1;
                                            $('#tab-widget').attr('data-in-list', row_id); 
                                            $('#wid-table-internal-services table tbody').prepend('' +
                                             '<tr data-key="'+$("#internal-service-list :selected").val()+'">' +
                                                '<td>' + $("#internal-service-list :selected").text() + '<input type="hidden" name="DocProduct['+row_id+'][product_id]" value="'+ $("#internal-service-list :selected").val() +'"><input type="hidden" name="DocProduct['+row_id+'][type]" value="3"></td>'
                                              + '<td>' + $("#internal-service-period-add").val() + '<input type="hidden" name="DocProduct['+row_id+'][period]" value="'+ $("#internal-service-period-add").val() +'"></td>'
                                              + '<td>' + $("#internal-service-quantity-add").val() + '<input type="hidden" name="DocProduct['+row_id+'][quantity]" value="'+ $("#internal-service-quantity-add").val() +'"></td>'
                                              + '<td>' + $("#internal-service-price-add").val() + '<input type="hidden" name="DocProduct['+row_id+'][price]" value="'+ $("#internal-service-price-add").val() +'"></td>'
                                              + '<td>'+((parseFloat($("#internal-service-price-add").val()) * parseFloat($("#internal-service-quantity-add").val())))+'<input type="hidden" name="DocProduct['+row_id+'][total_sum]" value="'+((parseFloat($("#internal-service-price-add").val()) * parseFloat($("#internal-service-quantity-add").val())))+'"  field="total_sum"></td>'
                                              + '<td><button type="button" id="table-del-button" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>'
                                              + '</tr>'
                                             );
                                             
                                         
                                             $("#internal-service-list").select2("val", "");
                                             $("#internal-service-quantity-add").val('');
                                             $("#internal-service-period-add").val('');
                                             $("#internal-service-price-add").val('');
                                  
                                             
                                             calculateSum();
                                              
                                }else {
                                    alert('Заполните значения !');
                                }
                                
                                }
                            );
             
                        });
JS
    , \yii\web\View::POS_END);



?>

<br>
    <div class="row">
        <div class="col-sm-12">
        <div class="form-group field-doc-event_name">
            <div class="col-md-4">
                    <label class="col-md-4 control-label" for="doc-event_name"><?= \Yii::t('app','Service') ?></label>
                    <div class="col-md-8">
                        <?= $internal_service_select ?>
                    </div>
            </div>
            <div class="col-md-8">

                    <div class="col-md-3">
                            <label class="col-md-6 control-label" for="internal-service-period-add">Период:</label>
                            <div class="col-md-6">
                               <input type="text" id="internal-service-period-add" class="form-control">
                            </div>
                    </div>
                    <div class="col-md-3">
                        <label class="col-md-6 control-label" for="internal-service-quantity-add">Кол-во:</label>
                        <div class="col-md-6">
                           <input type="text" id="internal-service-quantity-add" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label class="col-md-6 control-label" for="internal-service-price-add">Цена:</label>
                        <div class="col-md-6">
                           <input type="text" id="internal-service-price-add" class="form-control">
                        </div>
                    </div>
                        <div class="col-md-3">
                           <?= $internal_service_button_add ?>
                         </div>
                    </div>

        </div>
        </div>
    </div>

<?= \yii\grid\GridView::widget([
    'dataProvider' => $dataInternalServiceProvider,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    //'caption' => $this->title,
    'emptyText' => false,
    'summary' => '',
    'options' => ['id' => 'wid-table-internal-services'],
    'columns' => [
        [
            'attribute' => 'product_id',
            'label' => Yii::t('app', 'Service'),
            'headerOptions' => ['style'=>'width: 50%'],
            'format' => 'raw',
            'value' => function($model) {
                if($model->product)
                    return $model->product->name . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][product_id]", $model->product->id ) .  \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][type]", $model->type );
            }
        ],
        [
            'attribute' => 'period',
            'headerOptions' => ['style'=>'width: 10%'],
            'format' => 'raw',
            'value' => function($model) {
                return $model->period . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][period]", $model->period );
            }
        ],
        [
            'attribute' => 'quantity',
            'headerOptions' => ['style'=>'width: 10%'],
            'format' => 'raw',
            'value' => function($model) {
                return $model->quantity . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][quantity]", $model->quantity );
            }                                ],
        [
            'attribute' => 'price',
            'headerOptions' => ['style'=>'width: 10%'],
            'format' => 'raw',
            'value' => function($model) {
                return $model->price . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][price]", $model->price );
            }
        ],
        [
            'attribute' => 'total_sum',
            'headerOptions' => ['style'=>'width: 10%'],
            'format' => 'raw',
            'value' => function($model) {
                return $model->total_sum . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][total_sum]", $model->total_sum, [ 'field' => "total_sum"]  );
            }                                ],
        [
            'headerOptions' => ['style'=>'min-width: 80px'],
            'options' => ['style'=>'min-width: 80px'],
            'format' => 'raw',
            'value' => function($model) {
                return '<button type="button" id="table-del-button" class="btn btn-danger"><i class="fa fa-trash"></i> </button>';
            }
        ]
    ]

]); ?>

