<?php
use \yii\web\JsExpression;
?>

<div class="row">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
    </div>
    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">

    <?= $form->field($model, 'files')->label(Yii::t('app', 'Files'))->widget(
        \trntv\filekit\widget\Upload::className(),
        [
            'url' => ['file-upload', 'download-file'],
            'sortable' => true,
            'maxFileSize' => 10000000, // 10 MiB
            'maxNumberOfFiles' => 10,
            'clientOptions' => [
                'done' => new JsExpression('function(e, data) { console.log(data);}'),
            ]
        ]); ?>
    </div>
</div>