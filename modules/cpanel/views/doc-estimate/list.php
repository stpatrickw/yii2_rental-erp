<?php
use yii\helpers\Html;
use yii\helpers\Url;

use app\extensions\smartadmin\widgets\SmartWidgetSection;
use app\extensions\smartadmin\widgets\SmartWidget;
use kartik\dynagrid\DynaGrid;

$this->title = \Yii::t('app', 'My estimates');
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

    <div class="row-margin row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?= Html::a('<i class="fa fa-plus"></i> '. \Yii::t('app','Add'), Url::to(['doc-estimate/create']) , ['class' => 'btn btn-success pull-right']); ?>
        </div>
    </div>

    <?php SmartWidgetSection::begin(); ?>

    <?php
    $columns = [
        [
            'attribute' => 'agentName',
            'headerOptions' => ['style'=>'width: 35%'],
           /* 'value' => function($model) {
                if($model->agent)
                    return $model->agent->agent_name;
                else return '';
            }*/
        ],
        [
            'attribute' => 'event_name',
            'headerOptions' => ['style'=>'width: 25%'],
        ],
        [
            'attribute' => 'period',
            'headerOptions' => ['style'=>'width: 10%'],
        ],
        [
            'attribute' => 'total_sum',
            'headerOptions' => ['style'=>'width: 15%'],
        ],
       /* [
            'attribute' => 'status_id',
            'headerOptions' => ['style'=>'width: 15%'],
            'format' => 'html',
            'value' => function($model) {
                if($model->status)
                    return $model->status->name;
                else return '';
            }
        ],*/
        [
            'class'=>'kartik\grid\ActionColumn',
            'dropdown'=>false,
            'headerOptions' => ['style'=>'min-width: 100px'],
            'order'=>DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-pencil"></span>', $url, ['class' => 'btn btn-success']);
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-trash"></span>', $url, ['class' => 'btn btn-danger']);
                },

            ],
        ],
        [
            'class'=>'kartik\grid\CheckboxColumn',  'order'=>DynaGrid::ORDER_FIX_RIGHT,
        ],
    ]; ?>


    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <?php
            SmartWidget::begin(['title' => $this->title, 'icon' => 'fa-table', 'editbutton' => false, 'deletebutton' => false, 'theme' => 'jarviswidget-color-darken']);

            echo DynaGrid::widget([
                'columns'=>$columns,
                'storage'=>DynaGrid::TYPE_COOKIE,
                'theme'=>'panel-default',
                'gridOptions'=>[
                    'dataProvider'=>$provider,
                    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                    //   'filterModel'=>$searchModel,
                    'panel'=>['heading'=> false, 'before' => false],
                ],
                'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
            ]);
            SmartWidget::end();

            ?>

        </article>
    </div>

<?php SmartWidgetSection::end(); ?>
