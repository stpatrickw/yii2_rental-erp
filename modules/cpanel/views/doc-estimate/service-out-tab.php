<?php

$supplier_select = \kartik\select2\Select2::widget([
    'name' => 'state_3',
    'id' => 'supplier-list',
    'value' => '',
    'data' => $listAgents,
    'options' => ['placeholder' => Yii::t('app', 'Choose supplier...')]
]);


$external_service_select = \kartik\select2\Select2::widget([
    'name' => 'state_2',
    'id' => 'external-service-list',
    'value' => '',
    'data' => $listServices,
    'options' => ['placeholder' => 'Выберите услугу...']
]);

$external_service_button_add = \yii\helpers\Html::Button('<i class="fa fa-plus"></i> '. \Yii::t('app','Add'),
    ['class' => 'btn btn-primary', 'id' => 'external-service-add-button']
);

$this->registerJs(<<<JS
                      $(document).ready(function() {   
                          $('#external-service-add-button').click(function() {
                                if($("#supplier-list :selected").val() && $("#external-service-list").val() 
                                    && $.isNumeric($("#external-service-period-add").val()) === true
                                    && $.isNumeric($("#external-service-quantity-add").val()) === true
                                    && $.isNumeric($("#external-service-price-add").val()) === true
                                    && $.isNumeric($("#external-service-price-supplier-add").val()) === true
                                    && $.isNumeric($("#external-service-vat-add").val()) === true ){ 
                                
                                row_id = parseInt($('#tab-widget').attr('data-in-list')) + 1;
                                            $('#tab-widget').attr('data-in-list', row_id); 
                                            $('#wid-table-external-services table tbody').prepend('' +
                                             '<tr data-key="'+$("#external-service-list :selected").val()+'">' +
                                                '<td>' + $("#supplier-list :selected").text() + '<input type="hidden" name="DocProduct['+row_id+'][agent_id]" value="'+ $("#supplier-list :selected").val() +'"><input type="hidden" name="DocProduct['+row_id+'][type]" value="2"></td>'
                                              + '<td>' + $("#external-service-list :selected").text() + '<input type="hidden" name="DocProduct['+row_id+'][product_id]" value="'+ $("#external-service-list :selected").val() +'"></td>'
                                              + '<td>' + $("#external-service-period-add").val() + '<input type="hidden" name="DocProduct['+row_id+'][period]" value="'+ $("#external-service-period-add").val() +'"></td>'
                                              + '<td>' + $("#external-service-quantity-add").val() + '<input type="hidden" name="DocProduct['+row_id+'][quantity]" value="'+ $("#external-service-quantity-add").val() +'"></td>'
                                              + '<td>' + $("#external-service-price-add").val() + '<input type="hidden" name="DocProduct['+row_id+'][price]" value="'+ $("#external-service-price-add").val() +'"></td>'
                                              + '<td>' + $("#external-service-price-supplier-add").val() + '<input type="hidden" name="DocProduct['+row_id+'][price_supplier]" value="'+ $("#external-service-price-supplier-add").val() +'"></td>'
                                              + '<td>' + $("#external-service-vat-add").val() + '<input type="hidden" name="DocProduct['+row_id+'][vat]" value="'+ $("#external-service-vat-add").val() +'"></td>'
                                              + '<td>0</td>'
                                              + '<td>'+ ((parseFloat($("#external-service-price-add").val()) * parseFloat($("#external-service-quantity-add").val())))+'<input type="hidden" name="DocProduct['+row_id+'][total_sum]" value="' + ((parseFloat($("#external-service-price-add").val()) * parseFloat($("#external-service-quantity-add").val())))+ '"  field="total_sum"></td>'
                                              + '<td><button type="button" id="table-del-button" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>'
                                              + '</tr>'
                                             );
                                             
                                             
                                             $("#supplier-list").select2("val", "");
                                             $("#external-service-list").select2("val", "");
                                             $("#external-service-quantity-add").val('');
                                             $("#external-service-period-add").val('');
                                             $("#external-service-price-supplier-add").val('');
                                             $("#external-service-price-add").val('');
                                             $("#external-service-vat-add").val('');
                                             
                                     calculateSum();
                                              
                                }else {
                                    alert('Заполните значения !');
                                }
                                
                                }
                            );
                            
       
                        });
JS
    , \yii\web\View::POS_END);

?>

<br>
                        <div class="row">
                        <div class="col-sm-12">
                        <div class="form-group field-doc-event_name">
                            <div class="col-md-5">
                                <div class="col-md-12">
                                    <label class="col-md-4 control-label" for="doc-event_name"><?= \Yii::t('app','Supplier') ?></label>
                                    <div class="col-md-8">
                                        <?= $supplier_select  ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="col-md-4 control-label" for="doc-event_name"><?= \Yii::t('app','Service') ?></label>
                                    <div class="col-md-8">
                                        <?= $external_service_select ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">

                                    <div class="col-md-4">
                                        <div class="col-md-12">
                                            <label class="col-md-6 control-label" for="external-service-period-add">Период:</label>
                                            <div class="col-md-6">
                                               <input type="text" id="external-service-period-add" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                        <label class="col-md-6 control-label" for="external-service-quantity-add">Кол-во:</label>
                                        <div class="col-md-6">
                                           <input type="text" id="external-service-quantity-add" class="form-control">
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="col-md-12">
                                        <label class="col-md-8 control-label" for="external-service-price-supplier-add">Цена поставщика</label>
                                        <div class="col-md-4">
                                           <input type="text" id="external-service-price-supplier-add" class="form-control">
                                        </div>
                                        </div>
                                        <div class="col-md-12">
                                        <label class="col-md-8 control-label" for="external-service-price-add">Цена продажи:</label>
                                        <div class="col-md-4">
                                           <input type="text" id="external-service-price-add" class="form-control">
                                        </div>
                                        </div>
                                        <div class="col-md-12">
                                        <label class="col-md-8 control-label" for="external-service-vat-add">НДС</label>
                                        <div class="col-md-4">
                                           <input type="text" id="external-service-vat-add" class="form-control">
                                        </div>
                                        </div>
                                    </div>
                                        <div class="col-md-3">
                                           <?= $external_service_button_add ?>
                                         </div>
                                    </div>

                        </div>
            </div>
    </div>

<?= \yii\grid\GridView::widget([
    'dataProvider' => $dataExternalServiceProvider,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    //'caption' => $this->title,
    'emptyText' => false,
    'summary' => '',
    'options' => ['id' => 'wid-table-external-services', 'data-external-services-in-list' => 0],
    'columns' => [
        [
            'attribute' => 'agent_id',
            'headerOptions' => ['style'=>'width: 20%'],
            'format' => 'raw',
            'value' => function($model) {
                if($model->product)
                    return ($model->agent ? $model->agent->fio : '') . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][product_id]", $model->product->id ) .  \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][type]", $model->type );
            }
        ],                                [
            'attribute' => 'product_id',
            'label' => Yii::t('app', 'Service'),
            'headerOptions' => ['style'=>'width: 20%'],
            'format' => 'raw',
            'value' => function($model) {
                if($model->product)
                    return $model->product->name . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][product_id]", $model->product->id );
            }
        ],
        [
            'attribute' => 'period',
            'headerOptions' => ['style'=>'width: 10%'],
            'format' => 'raw',
            'value' => function($model) {
                return $model->period . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][period]", $model->period );
            }
        ],
        [
            'attribute' => 'quantity',
            'headerOptions' => ['style'=>'width: 10%'],
            'format' => 'raw',
            'value' => function($model) {
                return $model->quantity . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][quantity]", $model->quantity );
            }                                ],
        [
            'attribute' => 'price',
            'headerOptions' => ['style'=>'width: 10%'],
            'format' => 'raw',
            'value' => function($model) {
                return $model->price . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][price]", $model->price );
            }
        ],
        [
            'attribute' => 'price_supplier',
            'headerOptions' => ['style'=>'width: 10%'],
            'format' => 'raw',
            'value' => function($model) {
                return $model->price_supplier . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][price_supplier]", $model->price_supplier );
            }
        ],
        [
            'attribute' => 'vat',
            'headerOptions' => ['style'=>'width: 10%'],
            'format' => 'raw',
            'value' => function($model) {
                return $model->price_supplier . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][price_supplier]", $model->price_supplier );
            }
        ],
        [
            'attribute' => 'debt',
            'headerOptions' => ['style'=>'width: 10%'],
            'format' => 'raw',
            'value' => function($model) {
                return ($model->agent ? $model->agent->debt : '') ;
            }
        ],
        [
            'attribute' => 'total_sum',
            'headerOptions' => ['style'=>'width: 20%'],
            'format' => 'raw',
            'value' => function($model) {
                return $model->total_sum . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][total_sum]", $model->total_sum, [ 'field' => "total_sum"]  );
            }                                ],
        [
            'headerOptions' => ['style'=>'min-width: 80px'],
            'options' => ['style'=>'min-width: 80px'],
            'format' => 'raw',
            'value' => function($model) {
                return '<button type="button" id="table-del-button" class="btn btn-danger"><i class="fa fa-trash"></i> </button>';
            }
        ]
    ]

]); ?>