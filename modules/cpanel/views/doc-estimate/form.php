<?php

use yii\web\JsExpression;

$this->title = \Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Estimate'),
    'url' => \yii\helpers\Url::to(['estimate/list'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<section id="widget-grid" class="">
    <?php $form = \yii\bootstrap\ActiveForm::begin([
        'id' => 'doc-estimate-form',
        'method' => 'post',
        'options' => [
            'class' => 'form-horizontal',
            'enctype'=>'multipart/form-data'],
        'validateOnChange' => true,
        'validateOnBlur' => true,
        'validateOnType'=> true,
    ]); ?>
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="true" data-widget-editbutton="true">
                <header>
                    <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                    <h2><?= \Yii::t('app', 'Estimate') ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">


                        <?= $form->field($model, 'id',
                                [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}',
                                    'labelOptions' => ['class' => 'col-md-2 control-label'],
                                    'options' => []
                                ])->textInput(['readonly' => 'true']);
                        ?>

                        <?php $mail_button = \yii\helpers\Html::a('<i class="fa fa-envelope-o"></i> ' . \Yii::t('app', 'Send email')  , '#' , ['class' => 'btn btn-success pull-right', 'style' => 'margin-right: 15px;']); ?>

                        <?= $form->field($model, 'doc_date',
                                [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}' . $mail_button,
                                    'labelOptions' => ['class' => 'col-md-2 control-label'],
                                ])->widget( \nex\datepicker\DatePicker::className(), [
                            'addon' => false,
                            'language' => 'ru',
                            'placeholder' => Yii::t('app', 'Choose date...'),
                            'size' => 'sm',
                            'clientOptions' => [
                                'format' => 'YYYY-MM-DD',
                                'stepping' => 30,
                            ],
                        ]);
                        ?>

                        <?= $form->field($model, 'event_name',
                            [   'template' => '{label}<div class="col-md-10">{input}</div>{hint}{error}',
                                'labelOptions' => ['class' => 'col-md-2 control-label'],
                            ])->textInput();
                        ?>

                        <?php $button_create = \yii\bootstrap\Modal::widget([
                            'id' => 'agent-modal',
                            'closeButton' => ['tag' => 'button', 'label' => 'close'],
                            'toggleButton' => [
                                'label' => '<i class="fa fa-plus"></i> ',
                                'tag' => 'a',
                                'data-target' => '#agent-modal',
                                'href' => \yii\helpers\Url::toRoute(['agent/create', 'callback' => 'createAgentCallback']),
                                'class' => 'btn btn-success pull-left',
                            ],
                            'clientOptions' => false,
                        ]); ?>

                        <?= $form->field($model, 'agent_id',
                                [   'template' => '{label}<div class="col-xs-8 col-sm-8 col-md-6">{input}</div>' . $button_create . '{hint}{error}',
                                    'labelOptions' => ['class' => 'col-xs-2 col-sm-2 col-md-2 control-label'],
                                ])->label(Yii::t('app', 'Customer'))->widget( \kartik\select2\Select2::classname(), [
                            'data' => $listAgents,
                            'language' => 'ru',
                            'options' => ['placeholder' => \Yii::t('app','Choose the customer...')],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            'pluginEvents' => [
                                "change" => "function(data) { fillAgentFields($('#doc-agent_id').val()); }",
                            ],
                        ]);
                        ?>

                        <?php
                        $this->registerJs(<<<JS
                            jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
                                 jQuery(".dynamicform_wrapper .dynamic-datepicker").each(function(index) {
                                    $("#productprice-"+index+"-product_price_date").datetimepicker(datetimepicker_b16a0957); 
                                 }); 
                            });
JS
                            , \yii\web\View::POS_END);
                        ?>

                        <div id="agent-contacts">
                        <?php if($model->agent) {
                            foreach ($model->agent->contacts as $contact) { ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"
                                                   for="agent-fio"><?= Yii::t('app', 'Fio') ?></label>
                                            <div class="col-md-2">
                                                <input type="text" id="agent-fio" class="form-control"
                                                       value="<?= $contact->fullName ?>"
                                                       readonly="true">
                                            </div>
                                            <label class="col-md-1 control-label"
                                                   for="agent-phone"><?= Yii::t('app', 'Phone') ?></label>
                                            <div class="col-md-2">
                                                <input type="text" id="agent-phone" class="form-control"
                                                       value="<?= $contact->phone ?>"
                                                       readonly="true">
                                            </div>
                                            <label class="col-md-2 control-label"
                                                   for="agent-email"><?= Yii::t('app', 'Email') ?></label>
                                            <div class="col-md-2">
                                                <input type="text" id="agent-email-test" class="form-control"
                                                       value="<?= $contact->email ?>"
                                                       readonly="true">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            <? }
                        } ?>
                        </div>


                        <?= $form->field($model, 'period',
                            [   'template' => '{label}<div class="col-md-1">{input}</div>{hint}{error}',
                                'labelOptions' => ['class' => 'col-md-2 control-label'],
                            ])->textInput();
                        ?>


                        <?php

                        $get_agent_detail_url = \yii\helpers\Url::to(['agent/details']);
                        $agent_fio = Yii::t('app', 'Fio');
                        $agent_phone = Yii::t('app', 'Phone');
                        $agent_email = Yii::t('app', 'Email');

                        $this->registerJs(<<<JS
                      
                      function fillAgentFields(id){
                                var data = { agent_id: id};
                                $.ajax({
                                          type: "POST",
                                          url: '$get_agent_detail_url',
                                          dataType: "json",
                                          data: data,
                                          success: function (response) {
                                         
                                            if(response.agent){
                                                html = '';
                                                
                                                $.each(response.contacts, function(index, element) {  
                                                      html += '<div class="row">'
                                                               + '<div class="col-sm-12">'
                                                               +     '<div class="form-group">'
                                                               +         '<label class="col-md-2 control-label"'
                                                               +         '       for="agent-fio">$agent_fio</label>'
                                                               +         '<div class="col-md-2">'
                                                               +         '    <input type="text"  class="form-control"'
                                                               +         '           value="'+ element.last_name +'"'
                                                               +         '           readonly="true">'
                                                               +         '</div>'
                                                               +         '<label class="col-md-1 control-label"'
                                                               +         '       for="agent-phone">$agent_phone</label>'
                                                               +         '<div class="col-md-2">'
                                                               +         '    <input type="text" class="form-control"'
                                                               +         '           value="'+ element.phone +'"'
                                                               +         '           readonly="true">'
                                                               +         '</div>'
                                                               +         '<label class="col-md-2 control-label"'
                                                               +         '       for="agent-email">$agent_email</label>'
                                                               +         '<div class="col-md-2">'
                                                               +         '    <input type="text"  class="form-control"'
                                                               +         '           value="'+ element.email +'"'
                                                               +         '           readonly="true">'
                                                               +         '</div>'
                            
                                                               +     '</div>'
                                                               + '</div>'
                                                           + '</div>';
                                                      
                                                });
                                                $('#agent-contacts').html(html);
                                            $('#agent-fio').val(response.agent.agent_name);
                                            }else{
                                              $('#agent-contacts').html('');  
                                            }
                                                                                        
                                          },
                                          error: function (xhr, ajaxOptions, thrownError) {
                                            $('#agent-contacts').html('');
                                          }
                                        });
                                                                                            
                      }
                      
                      function calculateSum() {
                      
                         var summa = 0; 
                      
                         $("input[field=total_sum]").each( function() {
                            summa = summa + parseFloat($(this).val());
                         })
                           
                         var skidka = parseFloat($('#doc-discount').val());  
                           
                         if (!isNaN(skidka)) {
                             summa = summa - skidka;
                         }
                         
                         $('#doc-total_sum').val(summa);                               
                        
                               
                        };
                        
                        $("#doc-discount").on('keyup paste', calculateSum);
                        
                        
JS
                            , \yii\web\View::POS_END);





                        echo \yii\bootstrap\Tabs::widget([
                        'options' => ['id' => 'tab-widget'],
                        'items' => [
                            [
                            'label' => \Yii::t('app', 'Equipment'),
                            'content' => $this->render('product-tab', ['listProducts' => $listProducts, 'dataProductsProvider' => $dataProductsProvider]),
                            'active' => true
                            ],
                            [
                            'label' => \Yii::t('app', 'External services'),
                            'content' => $this->render('service-out-tab', ['listAgents' => $listAgents, 'listServices' => $listServices,'dataExternalServiceProvider' => $dataExternalServiceProvider]),
                         //   'active' => true,
                            //'headerOptions' => [],
                            ],
                            [
                            'label' => \Yii::t('app', 'Internal services'),
                            'content' => $this->render('service-in-tab', ['listServices' => $listServices, 'dataInternalServiceProvider' => $dataInternalServiceProvider]),
                           // 'active' => true,
                            //'headerOptions' => [],
                            ],
                            [
                            'label' => \Yii::t('app', 'Files'),
                            'content' => $this->render('files-tab', ['form' => $form, 'model' => $model]),
                           // 'active' => true,
                            //'headerOptions' => [],
                            ],
                        ],
                        ]);

                        ?>

                        <div class="row-margin">
                        <?= $form->field($model, 'discount',
                                [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}',
                                    'labelOptions' => ['class' => 'col-md-2 control-label'],
                                ])->textInput();
                        ?>

                        <?= $form->field($model, 'total_sum',
                                [   'template' => '{label}<div class="col-md-2">{input}</div>{hint}{error}',
                                    'labelOptions' => [ 'class' => 'col-md-2 control-label',
                                                        'style' => 'font-weight: bold;'],
                                ])->textInput(['readonly' => 'true']);
                        ?>
                        </div>

                    </div>
                </div>

            </div>

        </article>
    </div>


    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <?= \yii\helpers\Html::submitButton('<i class="fa fa-save"></i> '. \Yii::t('app','Save'),
                    ['class' => 'btn btn-primary']
                ); ?>

                <?= \yii\helpers\Html::a('<i class="fa fa-angle-left"></i> '. \Yii::t('app','Back'), \yii\helpers\Url::to(['doc-estimate/list']) , ['class' => 'btn btn-success pull-left']); ?>
            </div>
        </div>
    </div>

    <?php $form->end();?>
</section>


<?php

$this->registerJs(<<<JS

    function createAgentCallback(id, value){
        $('#doc-agent_id').append($("<option></option>").attr("value", id).text(value)); 
        $('#doc-agent_id').val(id).change();
        fillAgentFields(id);
    
    }  



JS
    , \yii\web\View::POS_END);

?>
