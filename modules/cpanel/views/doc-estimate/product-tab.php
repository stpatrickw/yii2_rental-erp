<?php

use app\components\widgets\Modal;

$product_select = \kartik\select2\Select2::widget([
    'name' => 'state_1',
    'id' => 'product-list',
    'value' => '',
    'data' => $listProducts,
    'options' => ['placeholder' => 'Выберите оборудование ...']
]);

$product_button_add = \yii\helpers\Html::Button('<i class="fa fa-plus"></i> '. \Yii::t('app','Add'),
    ['class' => 'btn btn-primary', 'id' => 'product-add-button']
);

$product_button_add_url = \yii\helpers\Url::to(['product/get-current-price']);

$this->registerJs(<<<JS
                       $(document).ready(function() {   
                          $('#product-add-button').click(function() {
                                add_product_to_list();                                
                                }
                            );
                            
                            $(document).on('click', '#table-del-button', function (e) {
                                $(e.currentTarget).parent().parent().remove();
                                calculateSum();
                            } )
                        });


    function add_product_to_list(){
        if($("#product-list :selected").val() && $.isNumeric($("#product-quantity-add").val()) === true && $.isNumeric($("#product-period-add").val()) === true ){
                                
                                data = {product: { 
                                          product_id: $("#product-list :selected").val(),
                                          quantity: $("#product-quantity-add").val(),
                                          period: $("#product-period-add").val() }},
                                $.ajax({
                                          type: "POST",
                                          url: '$product_button_add_url',
                                          dataType: "json",
                                          data: data,
                                          success: function (response) {
                                            console.log(response);
                                            row_id = parseInt($('#tab-widget').attr('data-in-list')) + 1;
                                            $('#tab-widget').attr('data-in-list', row_id); 
                                            $('#wid-table-products table tbody').prepend('' +
                                             '<tr data-key="'+$("#product-list :selected").val()+'">' +
                                                '<td>' + response.name + '<input type="hidden" name="DocProduct['+row_id+'][product_id]" value="'+ $("#product-list :selected").val() +'"><input type="hidden" name="DocProduct['+row_id+'][type]" value="1"></td>'
                                              + '<td>' + response.period + '<input type="hidden" name="DocProduct['+row_id+'][period]" value="'+ response.period +'"></td>'
                                              + '<td>' + response.quantity + '<input type="hidden" name="DocProduct['+row_id+'][quantity]" value="'+ response.quantity +'"></td>'
                                              + '<td>' + response.price + '<input type="hidden" name="DocProduct['+row_id+'][price]" value="'+ response.price +'"></td>'
                                              + '<td>' + response.total_sum + '<input type="hidden" name="DocProduct['+row_id+'][total_sum]" value="'+ response.total_sum +'" field="total_sum"></td>'
                                              + '<td><button type="button" id="table-del-button" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>'
                                              + '</tr>'
                                             );
                                           //  $("#product-list").select2("val", "");
                                             $("#product-list").val('').change();
                                             $("#product-quantity-add").val('');
                                             $("#product-period-add").val('');
                                             $("#product-list").select2('focus');
                                             
                                             calculateSum();
                                          },
                                          error: function (xhr, ajaxOptions, thrownError) {
                                             alert(xhr.responseText);
                                          }
                                        });
                                                             
                                }else {
                                    alert('Заполните значения !');
                                }       
    }

    function add_product_press(e){
        if(e.keyCode === 13){
            add_product_to_list();
            e.preventDefault();
        }
    }

JS
    , \yii\web\View::POS_END);

?>

<br>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group field-doc-event_name">
            <label class="col-md-2 control-label" for="doc-event_name">Оборудование:</label>
            <div class="col-md-4">
                <?= $product_select ?>
                <!--<div class="col-md-12">

                </div>
                <div class="col-md-2">
                <?= Modal::widget([
                    'id' => 'product-modal-create',
                    'closeButton' => ['tag' => 'button', 'label' => 'close'],
                    'options' => ['tabindex' => false],
                    'size' => Modal::SIZE_LARGE,
                    'toggleButton' => [
                        'label' => '<i class="fa fa-plus"></i>',
                        'tag' => 'a',
                        'data-target' => '#product-modal-create',
                        'href' => \yii\helpers\Url::toRoute(['product/product-create', 'callback' => 'createProductCallback']),
                        'class' => 'btn btn-success',
                        'style' => 'margin-right: 10px'

                    ],
                    'clientOptions' => false,
                ]); ?>
                </div>
                --->
            </div>

            <label class="col-md-1 control-label" for="product-period-add">Период:</label>
            <div class="col-md-1">
                <input type="text" id="product-period-add" class="form-control" onkeypress="add_product_press(event)">
            </div>
            <label class="col-md-1 control-label" for="product-quantity-add"">Кол-во:</label>
            <div class="col-md-1">
                <input type="text" id="product-quantity-add" class="form-control" onkeypress="add_product_press(event)">
            </div>
            <div class="col-md-1">
                <?= $product_button_add ?>
            </div>
        </div>
    </div>
</div>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $dataProductsProvider,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    //'caption' => $this->title,
    'emptyText' => false,
    'summary' => '',
    'options' => ['id' => 'wid-table-products',
    'class' => 'jarviswidget jarviswidget-color-darken',
    'data-widget-colorbutton' => 'true',
    'data-product-in-list' => '0',
    'data-widget-editbutton' => 'true'],
    'columns' => [
        [
            'attribute' => 'product_id',
            'headerOptions' => ['style'=>'width: 30%'],
            'format' => 'raw',
            'value' => function($model) {
            if($model->product)
            return $model->product->name . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][product_id]", $model->product->id ) . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][type]", $model->type );
            }
        ],
        [
            'attribute' => 'period',
            'headerOptions' => ['style'=>'width: 15%'],
            'format' => 'raw',
            'value' => function($model) {
            return $model->period . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][period]", $model->period );
            }
        ],
        [
            'attribute' => 'quantity',
            'headerOptions' => ['style'=>'width: 15%'],
            'format' => 'raw',
            'value' => function($model) {
            return $model->quantity . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][quantity]", $model->quantity );
            }                                ],
        [
            'attribute' => 'price',
            'headerOptions' => ['style'=>'width: 15%'],
            'format' => 'raw',
            'value' => function($model) {
            return $model->price . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][price]", $model->price );
            }                                ],
        [
            'attribute' => 'total_sum',
            'headerOptions' => ['style'=>'width: 15%'],
            'format' => 'raw',
            'value' => function($model) {
            return $model->total_sum . \yii\helpers\Html::input('hidden', "DocProduct[{$model->id}][total_sum]", $model->total_sum, [ 'field' => "total_sum"] );
            }                                ],
        [
            'headerOptions' => ['style'=>'min-width: 80px'],
            'options' => ['style'=>'min-width: 80px'],
            'format' => 'raw',
            'value' => function($model) {
            return '<button type="button" id="table-del-button" class="btn btn-danger"><i class="fa fa-trash"></i> </button>';
            }
        ]
    ]

]); ?>


<?php

$this->registerJs(<<<JS

    function createProductCallback(id, value){
        $('#product-list').append($("<option></option>").attr("value", id).text(value)); 
        $('#product-list').val(id).change();
        //fillAgentFields(id);
    
    }  



JS
    , \yii\web\View::POS_END);

?>
