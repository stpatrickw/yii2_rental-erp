<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;

$this->title = \Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Agent'),
    'url' => \yii\helpers\Url::to(['agent/list'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<section id="widget-grid" class="">
    <?php $form = \yii\widgets\ActiveForm::begin([
        'id' => 'agent-form' . $model->id,
        'method' => 'post',
        'options' => [
            'class' => 'form-horizontal',
            'enctype'=>'multipart/form-data',
            ],
        'validateOnChange' => true,
        'validateOnBlur' => true,
        'validateOnType'=> true,
       // 'validateOnSubmit' => true,
        'enableAjaxValidation' => true,
        'validationUrl' => \yii\helpers\Url::to(['agent/validate'])
    ]); ?>
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="true" data-widget-editbutton="true">
                <header>
                    <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                    <h2><?= \Yii::t('app', 'Agent') ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                    <?php $general_tab = '
                    <div class="row">   
                      <div class="col-md-6">' .
                         $form->field($model, 'agent_name_short',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $model->getAttributeLabel( 'agent_name_short' )])
                    . '</div>
                        <div class="col-md-6">' .
                         $form->field($model, 'agent_name',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $model->getAttributeLabel( 'agent_name' )])
                    . '</div>
                    </div>
                    <div class="row">   
                      <div class="col-md-6">' .
                        $form->field($model, 'address_post',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $model->getAttributeLabel( 'address_post' )])
                        . '</div>
                        <div class="col-md-6">' .
                        $form->field($model, 'address_legal',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $model->getAttributeLabel( 'address_legal' )])
                        . '</div>
                    </div>
                    <div class="row">   
                      <div class="col-md-6">' .
                        $form->field($model, 'phone',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $model->getAttributeLabel( 'phone' )])
                        . '</div>
                        <div class="col-md-6">' .
                        $form->field($model, 'email',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $model->getAttributeLabel( 'email' )])
                        . '</div>
                    </div>
                    <div class="row">   
                      <div class="col-md-6">' .
                        $form->field($model, 'inn',
                            [   'enableAjaxValidation' => true,
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $model->getAttributeLabel( 'inn' )])
                        . '</div>
                        <div class="col-md-3">' .
                        $form->field($model, 'is_customer',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->checkbox()
                        . '</div>
                        <div class="col-md-3">' .
                        $form->field($model, 'is_supplier',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->checkbox()
                        . '</div>
                        <div class="col-md-3">' .
                        $form->field($model, 'use_vat',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->checkbox()
                        . '</div>
                    </div>
                     <div class="row">   
                      <div class="col-md-12">' .
                        $form->field($model, 'description',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textarea()
                     . '</div>
                    </div>';

                    $bank_tab = '
                    <div class="row">   
                      <div class="col-md-12">' .
                        $form->field($model, 'bank_name',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $model->getAttributeLabel( 'bank_name' )])
                        . '</div>
                    </div>
                    <div class="row">   
                      <div class="col-md-6">' .
                        $form->field($model, 'bank_rs',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $model->getAttributeLabel( 'bank_rs' )])
                        . '</div>
                        <div class="col-md-6">' .
                        $form->field($model, 'bank_kors',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $model->getAttributeLabel( 'bank_kors' )])
                        . '</div>
                    </div>
                    <div class="row">   
                      <div class="col-md-6">' .
                        $form->field($model, 'bank_bik',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $model->getAttributeLabel( 'bank_bik' )])
                        . '</div>
                        <div class="col-md-6">' .
                        $form->field($model, 'bank_oktmo',
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $model->getAttributeLabel( 'bank_oktmo' )])
                        . '</div>
                    </div>'
                    ?>

                    <?php
                    $tab_persons_items = '';
                    foreach ($modelsContact as $index => $modelContact) {
                        $tab_persons_items .= "<div class='item' style='border-bottom: solid #bbbbbb 1px; margin-bottom: 5px;'>";
                        $tab_persons_items .= "<div class='row'><div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'><button type=\"button\" data-toggle=\"tooltip\" title=\"\" class=\"btn btn-danger pull-right remove-item\" data-original-title=\"Add\"><i class=\"fa fa-trash\"></i></button></div></div>";

                        $tab_persons_items .= "<div class='row'>";
                        $tab_persons_items .= "<div class=\"col-md-4\">";
                        $tab_persons_items .= $form->field($modelContact, "[{$index}]last_name",
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $modelContact->getAttributeLabel( 'last_name' )]);
                        $tab_persons_items .= "</div>";
                        $tab_persons_items .= "<div class=\"col-md-4\">";
                        $tab_persons_items .= $form->field($modelContact, "[{$index}]first_name",
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $modelContact->getAttributeLabel( 'first_name' )]);
                        $tab_persons_items .= "</div>";
                        $tab_persons_items .= "<div class=\"col-md-4\">";
                        $tab_persons_items .= $form->field($modelContact, "[{$index}]middle_name",
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $modelContact->getAttributeLabel( 'middle_name' )]);
                        $tab_persons_items .= "</div>";
                        $tab_persons_items .= "</div>";

                        $tab_persons_items .= "<div class='row'>";
                        $tab_persons_items .= "<div class=\"col-md-4\">";
                        $tab_persons_items .= $form->field($modelContact, "[{$index}]position",
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $modelContact->getAttributeLabel( 'position' )]);
                        $tab_persons_items .= "</div>";
                        $tab_persons_items .= "<div class=\"col-md-4\">";
                        $tab_persons_items .= $form->field($modelContact, "[{$index}]phone",
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $modelContact->getAttributeLabel( 'phone' )]);
                        $tab_persons_items .= "</div>";
                        $tab_persons_items .= "<div class=\"col-md-4\">";
                        $tab_persons_items .= $form->field($modelContact, "[{$index}]email",
                            [
                                'labelOptions' => ['class' => ''],
                                'options' => ['class'=> 'form-group', 'style' => 'margin: 0; text-align: left;']
                            ])->textInput(['placeholder' => $modelContact->getAttributeLabel( 'email' )]);
                        $tab_persons_items .= "</div>";
                        $tab_persons_items .= "</div>";

                        // necessary for update action.
                        /* if (! $modelEconomic->isNewRecord) {
                             echo \yii\helpers\Html::activeHiddenInput($modelEconomic, "[{$index}]id");
                         }*/

                        $tab_persons_items .= "</div>";
                    } ?>

                    <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                        'widgetBody' => '.container-items', // required: css class selector
                        'widgetItem' => '.item', // required: css class
                        'limit' => 10, // the maximum times, an element can be cloned (default 999)
                        'min' => 0, // 0 or 1 (default 1)
                        'insertButton' => '.add-item', // css class
                        'deleteButton' => '.remove-item', // css class
                        'model' => $modelsContact[0],
                        'formId' => 'agent-form' . $model->id,
                        'formFields' => [
                            'first_name',
                            'last_name',
                            'middle_name',
                            'phone',
                            'email',
                            'position'
                        ],
                    ]); ?>

                    <?php
                    $tab_persons =
                         '<div class="container-items">'
                        . $tab_persons_items
                        .
                        '</div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <button type="button" data-toggle="tooltip" title="" class="btn btn-primary pull-right add-item" data-original-title="Add"><i class="fa fa-plus-circle"></i></button>
                            </div>
                        </div>'; ?>


                        <?= \yii\bootstrap\Tabs::widget([
                            'options' => ['id' => 'tab-widget' . $model->id, 'style' => 'margin-bottom: 15px'],
                            'items' => [
                                [
                                    'label' => \Yii::t('app', 'General'),
                                    'content' => $general_tab,
                                    'active' => true
                                ],
                                [
                                    'label' => \Yii::t('app', 'Bank details'),
                                    'content' => $bank_tab,
                                   //  'active' => true,
                                    //'headerOptions' => [],
                                ],
                                [
                                    'label' => \Yii::t('app', 'Contact persons'),
                                    'content' => $tab_persons,
                                    //   'active' => true,
                                    //'headerOptions' => [],
                                ],

                            ],
                        ]); ?>

                        <?php DynamicFormWidget::end(); ?>

                    </div>
                </div>

            </div>

        </article>
    </div>

    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <?= \yii\bootstrap\Html::button(\Yii::t('app','Save'), ['onclick' => 'submitFormButton('.$callback.')', 'class' => 'showModalButton btn btn-success']); ?>

                <?= \yii\helpers\Html::button('<i class="fa fa-angle-left"></i> '. \Yii::t('app','Back'),
                    [
                        'class' => 'btn btn-success pull-left',
                        'data-dismiss' => "modal" ,
                        'aria-hidden' => "true"
                    ]);
                ?>

            </div>
        </div>
    </div>

    <?php
    $agent_save_url = ($model->isNewRecord ? \yii\helpers\Url::to(['agent/create']) : \yii\helpers\Url::to(['agent/update', 'id' => $model->id]));
    $inn_validate_message = \Yii::t('app', 'Inn already exist');

    $this->registerJs(<<<JS

    function submitFormButton(callback){
        var form = $("#agent-form$model->id"), 
            data = form.data("yiiActiveForm");
            $.each(data.attributes, function() {
                this.status = 3;
            });
            form.yiiActiveForm("validate");
                 
            if(!form.find(".has-error").length) {
                 
                  $.ajax({
                  url: '$agent_save_url',
                  type: 'post',
                  data: form.serialize(),
                  success: function (response) {
                     console.log(response);                   
                   
                   if(response.error){
                        form.find('.field-agent-inn').removeClass('has-success').addClass('has-error');
                        form.find('.field-agent-inn .help-block').html('$inn_validate_message');
                       
                   }
                   
                    if(!response.error){
                        form.closest('.modal').modal('hide'); 
                        setTimeout(callback(response.id, $('#agent-agent_name').val()), 200);
                    }

                  }
             });    
             }
             
                    
    }


   


JS
    , \yii\web\View::POS_END);

    ?>

    <?php $form->end();?>
</section>

<?php


?>

