<?php
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use app\extensions\smartadmin\widgets\SmartWidgetSection;
use app\extensions\smartadmin\widgets\SmartWidget;
use yii\widgets\Pjax;

$this->title = \Yii::t('app', 'Price types');
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="row-margin row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php echo $button_create = \yii\bootstrap\Modal::widget([
            'id' => 'price-types-modal-create',
            'closeButton' => ['tag' => 'button', 'label' => 'close'],
            'toggleButton' => [
                'label' => '<i class="fa fa-plus"></i> ' . \Yii::t('app','Add'),
                'tag' => 'a',
                'data-target' => '#price-types-modal-create',
                'href' => \yii\helpers\Url::toRoute(['price-type/create', 'callback' => 'refreshCallback']),
                'class' => 'btn btn-success pull-right',
            ],
            'clientOptions' => false,
        ]); ?>
    </div>
</div>

<?php SmartWidgetSection::begin(); ?>


<?php
$columns = [
    [
        'attribute' => 'name',
        'headerOptions' => ['style'=>'width: 25%'],
    ],
    [
        'attribute' => 'description',
        'headerOptions' => ['style'=>'width: 20%'],
        'format' => 'html',
        'value'=>function ($model) {
            return \yii\helpers\Html::decode($model->description);
        },
    ],
    [
        'attribute' => 'for_product',
        'headerOptions' => ['style'=>'width: 15%'],
        'format' => 'html',
        'value'=>function ($model) {
            return \yii\helpers\Html::decode($model->forProductStatus);
        },
    ],
    [
        'attribute' => 'for_service',
        'headerOptions' => ['style'=>'width: 15%'],
        'format' => 'html',
        'value'=>function ($model) {
            return \yii\helpers\Html::decode($model->forServiceStatus);
        },
    ],
    [
        'attribute' => 'is_dry',
        'headerOptions' => ['style'=>'width: 15%'],
        'format' => 'html',
        'value'=>function ($model) {
            return \yii\helpers\Html::decode($model->dryStatus);
        },
    ],
    [
        'attribute' => 'use_range',
        'headerOptions' => ['style'=>'width: 15%'],
        'format' => 'html',
        'value'=>function ($model) {
            return \yii\helpers\Html::decode($model->useRangeStatus);
        },
    ],
    [
        'attribute' => 'range_min',
        'headerOptions' => ['style'=>'width: 15%'],
    ],
    [
        'attribute' => 'range_max',
        'headerOptions' => ['style'=>'width: 15%'],
    ],
    [
        'class'=>'kartik\grid\ActionColumn',
        'dropdown'=>false,
        'headerOptions' => ['style'=>'min-width: 100px'],
        'order'=>DynaGrid::ORDER_FIX_RIGHT,
        'template' => '{update} {delete}',
        'buttons' => [
            'update' => function ($url, $model, $key) {

                $button_update = \yii\bootstrap\Modal::widget([
                    'id' => 'price-types-modal-update'.$model->id,
                    'closeButton' => ['tag' => 'button', 'label' => 'close'],
                    'toggleButton' => [
                        'label' => '<i class="fa fa-pencil"></i> ',
                        'tag' => 'a',
                        'data-target' => '#price-types-modal-update'.$model->id,
                        'href' => \yii\helpers\Url::toRoute(['price-type/update', 'id' => $model->id, 'callback' => 'refreshCallback']),
                        'class' => 'btn btn-success pull-left',
                    ],
                    'clientOptions' => false,
                ]);
                return $button_update;
                //  return Html::a('<span class="fa fa-pencil"></span>', $url, ['class' => 'btn btn-success']);
            },
            'delete' => function ($url, $model, $key) {
                $button_delete = \yii\bootstrap\Modal::widget([
                    'id' => 'price-types-modal-delete' . $model->id,
                    'options' => ['class' => 'bootstrap-dialog type-danger fade'],
                    'closeButton' => ['tag' => 'button', 'label' => 'x'],
                    'toggleButton' => [
                        'label' => '<i class="fa fa-trash"></i> ',
                        'tag' => 'a',
                        'data-target' => '#price-types-modal-delete'.$model->id,
                        'href' => \yii\helpers\Url::toRoute(['confirm/index', 'model' => 'price-type', 'id' => $model->id, 'action' => 'delete', 'callback' => 'refreshCallback']),
                        'class' => 'btn btn-danger',
                    ],
                    'clientOptions' => false,
                ]);
                return $button_delete;
            },

        ],
    ],
    [
        'class'=>'kartik\grid\CheckboxColumn',  'order'=>DynaGrid::ORDER_FIX_RIGHT,
    ],
]; ?>


<div class="row">
    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <?php
        SmartWidget::begin(['title' => $this->title, 'icon' => 'fa-table', 'editbutton' => false, 'deletebutton' => false, 'theme' => 'jarviswidget-color-darken']);
        ?>

        <?php Pjax::begin(['id' => 'pjax-price-types' ]) ?>

        <?php echo DynaGrid::widget([
            'columns'=>$columns,
            'storage'=>DynaGrid::TYPE_COOKIE,
            'theme'=>'panel-default',
            'gridOptions'=>[
                'dataProvider'=> $provider,
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                //   'filterModel'=> $searchModel,
                'panel'=>['heading'=> false, 'before' => false],
            ],
            'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
        ]); ?>

        <?php Pjax::end() ?>
        <?php SmartWidget::end(); ?>



    </article>
</div>
<?php SmartWidgetSection::end(); ?>

<?php

$this->registerJs(<<<JS

    function refreshCallback(){
        $.pjax.reload({container:"#pjax-price-types"});  //Reload GridView    
    }  


JS
    , \yii\web\View::POS_END);

?>



