<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\cpanel\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CpanelAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/cpanel/web';
    public $css = [
        'css/custom.css'
    ];
    public $js = [
        'js/dynamic-form-fixed.js'

    ];
    public $depends = [
      //  'yii\web\YiiAsset',
      //  'yii\bootstrap\BootstrapAsset',
    ];
}
