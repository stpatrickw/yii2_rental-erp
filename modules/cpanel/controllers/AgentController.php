<?php
namespace app\modules\cpanel\controllers;

use app\models\Agent;
use app\models\AgentContact;
use app\models\search\AgentSearch;
use app\modules\cpanel\components\CpanelController;
use app\modules\cpanel\models\Model;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\web\Response;

class AgentController extends CpanelController
{
    public $defaultAction = 'list';
    private $modelName = '';

    public function init()
    {
        $this->modelName = Agent::className();
        parent::init();
    }

    public function actionCreate()
    {
        return $this->actionUpdate(true);
    }

    public function actionUpdate($new = false)
    {

        $modelsContact =[];

        if($new === true){
            $model = $this->createModel($this->modelName);
            $modelsContact = [new AgentContact()];
            $model->is_customer = 1;
        }else{
            $model = $this->findModel($this->modelName, \Yii::$app->request->get('id'));
            foreach ($model->contacts as $contact){
                $modelsContact[] = $contact;
            }
            if(!$modelsContact) $modelsContact = [new AgentContact()];
        }

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $modelsContact = Model::createMultiple(AgentContact::classname());

            Model::loadMultiple($modelsContact, \Yii::$app->request->post());

             if ($model->save()) {
                 $model->deleteContacts();
                 foreach ($modelsContact as $item) {
                     $item->link('agent', $model);
                     $item->save(false);
                 }
                 $this->setFlash('success', \Yii::t('app', 'Modifications have been saved'));
            } else {
                $this->setFlash('error', \Yii::t('app', 'Modifications have not been saved'));
            }
            if(!\Yii::$app->request->isAjax) {
                return $this->redirect('list');
            } else {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return ['id' => $model->id];
            }
        }

        if(\Yii::$app->request->isAjax)
            if($model->errors) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $result = array('error' => 'true');
                return array_merge($result, ActiveForm::validate($model));
            } else
                return $this->renderAjax('form', ['model' => $model, 'modelsContact' => $modelsContact, 'callback' => \Yii::$app->request->get('callback')]);

    }

    public function actionValidate()
    {
        $model = $this->createModel($this->modelName);

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

    }

    public function actionDetails(){
        if(\Yii::$app->request->isAjax) {
            $post = \Yii::$app->request->post();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $agent = Agent::findOne($post['agent_id']);
            return ['agent' => $agent, 'contacts' => ($agent ? $agent->contacts : null)];
        }
    }

    public function actionDelete($id)
    {
        if($this->findModel($this->modelName, $id)->delete() !== false)
            $this->setFlash('success', \Yii::t('app', 'Item deleted successfully !'));
        else
            $this->setFlash('error', \Yii::t('app', 'Item has not been deleted !'));

        return $this->redirect(['list']);

    }

    public function actionList(){

        $search = new Agent();
        $provider = $search->search(\Yii::$app->request->get());


        return $this->render('list', ['provider' => $provider, 'searchModel' => $search]);

    }

}