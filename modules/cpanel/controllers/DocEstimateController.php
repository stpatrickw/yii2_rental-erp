<?php
namespace app\modules\cpanel\controllers;

use app\models\Agent;
use app\models\CategoryProduct;
use app\models\Doc;
use app\models\DocProduct;
use app\models\Product;
use app\models\Status;
use app\modules\cpanel\components\CpanelController;
use app\modules\cpanel\models\Model;

use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

class DocEstimateController extends CpanelController
{
    public $defaultAction = 'list';
    private $modelName = '';
    private $doc_type_id = 5;

    public function actions()
    {
        return [
            'file-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'uploads-delete',
            ],
            'uploads-delete' => [
                'class' => DeleteAction::className()
            ],
        ];
    }

    public function init()
    {
        $this->modelName = Doc::className();
        parent::init();
    }

    public function actionCreate()
    {
        return $this->actionUpdate(true);
    }

    public function actionUpdate($new = false)
    {

        $listAgents = [];
        $listProducts = [];
        $listServices = [];
        $agents = Agent::find()->orderBy('agent_name_short')->customers()->all();
        foreach ($agents as $agent) {
            $listAgents[$agent->id] = $agent->agent_name; // . ' (' . $agent->last_name . ($agent->first_name ? ' ' . $agent->first_name : '') . ')';
        }

        $modelsProduct =[];

        $productCategories = CategoryProduct::find()->orderBy('name')->all();
        foreach ($productCategories as $productCategory) {
            $products = Product::find()->where(['category_id' => $productCategory->id])->product()->orderBy('name')->all();
            $catProducts = [];
            foreach ($products as $product) {
                $catProducts[$product->id] = $product->name;
            }
            if($catProducts) {
                $listProducts[$productCategory->name . "(". count($catProducts) .")"] = $catProducts;
            }
            $services = Product::find()->where(['category_id' => $productCategory->id])->service()->orderBy('name')->all();
            $catServices = [];
            foreach ($services as $service) {
                $catServices[$service->id] = $service->name;
            }

            if($catServices) {
                $listServices[$productCategory->name . "(". count($catServices) .")"] = $catServices;
            }
        }

        if($new === true){
            $model = $this->createModel($this->modelName);
            $model->setDocType($this->doc_type_id);
            $modelsProduct = [new DocProduct()];
        }else{
            $model = $this->findModel($this->modelName, \Yii::$app->request->get('id'));
            foreach ($model->products as $product){
                $modelsProduct[] = $product;
            }
            if(!$modelsProduct) $modelsProduct = [new DocProduct()];
        }

        $statuses =  \yii\helpers\ArrayHelper::map(Status::find()->orderBy('sort')->asArray()->all(), 'id', 'name');

        if ($model->load(\Yii::$app->request->post())) {

            $model->setDocType($this->doc_type_id);
            $modelsProduct = array();
            if (isset(\Yii::$app->request->post()['DocProduct'])) {
                foreach (\Yii::$app->request->post()['DocProduct'] as $key => $postProduct) {
                $modP = new DocProduct();
                if (!$modP->load(array('DocProduct' => $postProduct))) {
                  //  load
                };
                $modelsProduct[] = $modP;
                }
            }

            if($model->validate() && Model::validateMultiple($modelsProduct)) {

                if ($model->save()) {
                    $model->deleteProducts();
                    foreach ($modelsProduct as $item) {
                        $item->link('doc', $model);
                        $item->save(false);
                    }
                    $this->setFlash('success', \Yii::t('app', 'Modifications have been saved'));
                } else {
                    $this->setFlash('error', \Yii::t('app', 'Modifications have not been saved'));
                }
                return $this->redirect('list');
            }
        }

        if($model->errors)  var_dump($model->errors);

        $dataProductsProvider = new ActiveDataProvider([
            'query' => DocProduct::find()->where(['doc_id' => $model->id]),
            'sort' => false,
        ]);
        $dataExternalServiceProvider = new ActiveDataProvider([
            'query' => DocProduct::find()->where(['doc_id' => $model->id]),
            'sort' => false,
        ]);
        $dataInternalServiceProvider = new ActiveDataProvider([
            'query' => DocProduct::find()->where(['doc_id' => $model->id]),
            'sort' => false,
        ]);

        return $this->render('form', [
                                        'model' => $model,
                                        'listAgents' => $listAgents,
                                        'listProducts' => $listProducts,
                                        'listServices' => $listServices,
                                        'dataProductsProvider' => $dataProductsProvider,
                                        'dataExternalServiceProvider' => $dataExternalServiceProvider,
                                        'dataInternalServiceProvider' => $dataInternalServiceProvider,
                                        'statuses' => $statuses
                                    ]);

    }

    public function actionDelete($id)
    {
        if($this->findModel($this->modelName, $id)->delete() !== false)
            $this->setFlash('success', \Yii::t('app', 'Modifications have been saved'));
        else
            $this->setFlash('error', \Yii::t('app', 'Modifications have not been saved'));

        return $this->redirect(['list']);

    }


    public function actionList(){

        $modelName = $this->modelName;

        return $this->render('list', ['provider' => $modelName::getProvider(5)]);

    }

}