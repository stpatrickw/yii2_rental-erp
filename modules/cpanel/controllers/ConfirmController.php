<?php
namespace app\modules\cpanel\controllers;

use app\modules\cpanel\components\CpanelController;
use yii\web\Response;

class ConfirmController extends CpanelController
{

    // action = delete, deleteAll
    public function actionIndex($model, $id, $action)
    {
        $action_url = \yii\helpers\Url::toRoute(["{$model}/{$action}", 'id' => $id]);
        return $this->renderAjax('form', ['id' => $id, 'action_url' => $action_url, 'callback' => \Yii::$app->request->get('callback') ]);
    }

}