<?php
namespace app\modules\cpanel\controllers;

use app\models\PriceType;
use app\modules\cpanel\components\CpanelController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class PriceTypeController extends CpanelController
{
    public $defaultAction = 'list';
    private $modelName = '';

    public function init()
    {
        $this->modelName = PriceType::className();
        parent::init();
    }

    public function actionCreate()
    {
        return $this->actionUpdate(true);
    }

    public function actionUpdate($new = false)
    {

        if($new === true){
            $model = $this->createModel($this->modelName);
        }else{
            $model = $this->findModel($this->modelName, \Yii::$app->request->get('id'));
        }

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {

            if ($model->save()) {
                $this->setFlash('success', \Yii::t('app', 'Modifications have been saved'));
            } else {
                $this->setFlash('error', \Yii::t('app', 'Modifications have not been saved'));
            }

        }


        if(\Yii::$app->request->isAjax) {
            if ($model->errors) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $result = array('error' => 'true');
                return array_merge($result, ActiveForm::validate($model));
            } else
                return $this->renderAjax('form', ['model' => $model, 'callback' => \Yii::$app->request->get('callback')]);
        }

    }

    public function actionDelete($id)
    {
        if($this->findModel($this->modelName, $id)->delete() !== false)
            $this->setFlash('success', \Yii::t('app', 'Modifications have been saved'));
        else
            $this->setFlash('error', \Yii::t('app', 'Modifications have not been saved'));

        return $this->redirect(['list']);

    }

    public function actionList(){

        $model = $this->createModel($this->modelName);

        return $this->render('list', ['provider' => $model->getProvider()]);

    }

}