<?php
namespace app\modules\cpanel\controllers;

use app\models\Pricelist;
use app\models\PriceType;
use app\models\Product;
use app\models\ProductPrice;
use app\modules\cpanel\components\CpanelController;
use app\modules\cpanel\models\Model;

use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\data\ActiveDataProvider;

class PricelistController extends CpanelController
{
    public $defaultAction = 'list';
    private $modelName = '';

    public function init()
    {
        $this->modelName = Pricelist::className();
        parent::init();
    }

    public function actionCreate()
    {
        return $this->actionUpdate(true);
    }

    public function actionUpdate($new = false)
    {
        $listProductPrices = [];
        $exclude_products = [];

        if($new === true){
            $model = $this->createModel($this->modelName);
            $model->pricelist_date = date("Y-m-d");
        }else{
            $model = $this->findModel($this->modelName, \Yii::$app->request->get('id'));
            $listProductPrices = $this->getProductPricesArray(\Yii::$app->request->get('id'));
            $exclude_p = ProductPrice::find()->where(['pricelist_id' => \Yii::$app->request->get('id')])->groupBy('product_id')->all();
            foreach ($exclude_p as $item) {
                $exclude_products[] = $item->product_id;
            }
        }

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {

            if ($model->save()) {
                $productPrices = \Yii::$app->request->post('ProductPrice');
                $model->deletePrices();
                if($productPrices) {
                    foreach ($productPrices as $product_id => $prices) {
                        foreach ($prices as $price_type_id => $price) {
                            if ($price) {
                                if ($pp_model = ProductPrice::find()->where(['product_id' => $product_id, 'pricelist_id' => $model->id, 'price_type_id' => $price_type_id])->one()) {
                                    if ($pp_model->price !== $price) {
                                        $pp_model->price = $price;
                                        $pp_model->save();
                                    }
                                } else {
                                    $pp_model = new ProductPrice();
                                    $pp_model->product_id = $product_id;
                                    $pp_model->pricelist_id = $model->id;
                                    $pp_model->price_type_id = $price_type_id;
                                    $pp_model->price = $price;
                                    $pp_model->save();
                                }
                            }
                        }
                    }
                }
                $this->setFlash('success', \Yii::t('app', 'Modifications have been saved'));
            } else {
                $this->setFlash('error', \Yii::t('app', 'Modifications have not been saved'));
            }
            return $this->redirect('list');

        }

        $dataProductsProvider = new ActiveDataProvider([
            'query' => ProductPrice::find(),
            'sort' => false,
        ]);

        return $this->render('form', [
            'model' => $model,
            'dataProductsProvider' => $dataProductsProvider,
            'listProductPrices' => $listProductPrices,
            'exclude_products' => $exclude_products]);

    }

    public function actionDelete($id)
    {
        if($this->findModel($this->modelName, $id)->delete() !== false)
            $this->setFlash('success', \Yii::t('app', 'Modifications have been saved'));
        else
            $this->setFlash('error', \Yii::t('app', 'Modifications have not been saved'));

        return $this->redirect(['list']);

    }


    public function actionList(){

        $model = $this->createModel($this->modelName);

        return $this->render('list', ['provider' => $model->getProvider()]);

    }

    public function getProductPricesArray($pricelist_id = 0){

        $products = ProductPrice::find()->joinWith('product')->where(['pricelist_id' => $pricelist_id])->groupBy('product_id')->orderBy('name')->all();
        $products_arr = [];
        foreach ($products as $product) {
            $product_prices = ProductPrice::find()->where(['pricelist_id' => $pricelist_id, 'product_id' => $product->product_id])->all();
            $prices_arr = [];
            foreach ($product_prices as $product_price) {
                $prices_arr[$product_price->price_type_id] = $product_price->price;
            }
            $products_arr[] = [
                'product' => [
                    'product_id' => $product->product_id,
                    'product_type_id' => $product->product->product_type_id,
                    'name' => $product->product->name
                ],
                'prices' => $prices_arr
            ];
        }

        return $products_arr;

    }

    public function actionAddone(){

        if(\Yii::$app->request->isAjax && \Yii::$app->request->post('add_product_id')) {

            \Yii::$app->response->format = Response::FORMAT_JSON;
            $price_types = PriceType::find()->orderBy('sort')->all();
            $product = Product::findOne(\Yii::$app->request->post('add_product_id'));
            $price_types_arr = [];
            $prices = [];
            foreach ($price_types as $price_type) {
                $price_types_arr[] = [
                    'id' => $price_type->id,
                    'name' => $price_type->name,
                    'use_range' => $price_type->use_range,
                    'is_dry' => $price_type->is_dry,
                    'for_product' => $price_type->for_product,
                    'for_service' => $price_type->for_service,
                    'product_price' => ($product ? $product->getLastPrice($price_type->id) : '')];
            }
            if($product) {

                $result = array('price_types' => $price_types_arr,
                    'product' => [
                        'id' => $product->id,
                        'product_type_id' => $product->product_type_id,
                        'name' => $product->name],
                    'prices' => $prices
                );
                return $result;
            }
        }

        return '';

    }

    public function actionAddall(){

        if(\Yii::$app->request->isAjax ) {

            \Yii::$app->response->format = Response::FORMAT_JSON;
            $price_types = PriceType::find()->orderBy('sort')->all();
            $post_products = \Yii::$app->request->post('products');
            $products = Product::find();
            if($post_products) $products = $products->where(['not in', 'id', $post_products]);
            $products = $products->orderBy('product_type_id, name')->all();
            $products_arr = [];
            foreach ($products as $product){
                $products_arr[] = [
                    'id' => $product->id,
                    'product_type_id' => $product->product_type_id,
                    'name' => $product->name,
                    'prices' => $product->lastPrices];
            }

            $price_types_arr = [];
            foreach ($price_types as $price_type) {
                $price_types_arr[] = [
                    'id' => $price_type->id,
                    'name' => $price_type->name,
                    'use_range' => $price_type->use_range,
                    'for_product' => $price_type->for_product,
                    'for_service' => $price_type->for_service,
                    'is_dry' => $price_type->is_dry];
            }
            if($products_arr) {
                $result = array('price_types' => $price_types_arr,
                    'products' => $products_arr
                );
                return $result;
            }
        }

        return '';

    }

}