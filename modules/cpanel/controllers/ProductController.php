<?php
namespace app\modules\cpanel\controllers;

use app\models\CategoryProduct;
use app\models\Product;
use app\models\ProductAttribute;
use app\models\ProductPrice;
use app\models\ProductVariant;
use app\modules\cpanel\components\CpanelController;
use app\modules\cpanel\models\Model;
use yii\data\ActiveDataProvider;

use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Response;
use yii\widgets\ActiveForm;

class ProductController extends CpanelController
{
    public $defaultAction = 'list';
    private $modelName = '';

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'uploads-delete',
                'validationRules' => [
                    ['file', 'image', 'extensions' => 'png, jpg'],
                ]
            ],
            'uploads-delete' => [
                'class' => DeleteAction::className()
            ],
        ];
    }

    public function init()
    {
        $this->modelName = Product::className();
        parent::init();
    }

    public function actionServiceCreate()
    {
        return $this->actionUpdate(true, 'service');
    }

    public function actionProductCreate()
    {
        return $this->actionUpdate(true, 'product');
    }

    public function actionUpdate($new = false, $type = 'product')
    {

        $listCategories = array( 0 => \Yii::t('app', 'Root category'));
        $modelsPrice = [];
        $modelsAttribute = [];
        $modelsVariant = [];

        $cats = CategoryProduct::find()->orderBy('name')->all();

        foreach ($cats as $cat) {
            $listCategories[$cat->id] = $cat->name;
        }

        if($new === true){
            $model = $this->createModel($this->modelName);
          //  $modelsPrice = [new ProductPrice()];
            $modelsAttribute = [new ProductAttribute()];
            $modelsVariant = [new ProductVariant()];

        }else{
            $model = $this->findModel($this->modelName, \Yii::$app->request->get('id'));
            if($model) {
                switch($model->product_type_id){
                    case Product::TYPE_PRODUCT: $type = 'product'; break;
                    case Product::TYPE_SERVICE: $type = 'service'; break;
                }
            }
           /* foreach ($model->prices as $price){
                $modelsPrice[] = $price;
            }
            if(!$modelsPrice) $modelsPrice = [new ProductPrice()];
*/
            foreach ($model->productAttributes as $attribute){
                $modelsAttribute[] = $attribute;
            }
            if(!$modelsAttribute) $modelsAttribute = [new ProductAttribute()];

            foreach ($model->productVariants as $variant){
                $modelsVariant[] = $variant;
            }
            if(!$modelsVariant) $modelsVariant = [new ProductVariant()];
        }


        if ($model->load(\Yii::$app->request->post()) ) {
          //  $modelsPrice = Model::createMultiple(ProductPrice::classname());
            $modelsAttribute = Model::createMultiple(ProductAttribute::classname());
            $modelsVariant = Model::createMultiple(ProductVariant::classname());

        //    Model::loadMultiple($modelsPrice, \Yii::$app->request->post());
            Model::loadMultiple($modelsAttribute, \Yii::$app->request->post());
            Model::loadMultiple($modelsVariant, \Yii::$app->request->post());

            if ( $model->validate()
                && Model::validateMultiple($modelsPrice)
                && Model::validateMultiple($modelsAttribute)
                && Model::validateMultiple($modelsVariant)) {

                if ($model->save()) {
                    //$model->deletePrices();
                    $model->deleteAttributes();
                    $model->deleteVariants();
                   /* foreach ($modelsPrice as $item) {
                        $item->link('product', $model);
                        $item->save(false);
                    } */
                    foreach ($modelsAttribute as $item) {
                        $item->link('product', $model);
                        $item->save(false);
                    }
                   // \Yii::info(ArrayHelper::toArray($modelsVariant), 'test');
                    foreach ($modelsVariant as $item) {
                        $item->link('product', $model);
                        $item->save(true);
                    }
                    $this->setFlash('success', \Yii::t('app', 'Modifications have been saved'));
                } else {
                    $this->setFlash('error', \Yii::t('app', 'Modifications have not been saved'));
                }
            }

            return $this->redirect('list');
        }

        if(\Yii::$app->request->isAjax)
            if($model->errors) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $result = array('error' => 'true');
                return array_merge($result, ActiveForm::validate($model));
            } else
                return $this->renderAjax(($type == 'product' ? 'product-form' : 'service-form'),
                    [
                        'model' => $model,
                        'listCategories' => $listCategories,
                        'modelsPrice' => $modelsPrice,
                        'modelsAttribute' => $modelsAttribute,
                        'modelsVariant' => $modelsVariant,
                        'callback' => \Yii::$app->request->get('callback')
                    ]);



      //  return $this->render(($type == 'product' ? 'product-form' : 'service-form'), ['model' => $model, 'listCategories' => $listCategories,
      //      'modelsPrice' => $modelsPrice]);

    }

    public function actionGetCurrentPrice(){
        if(!\Yii::$app->request->isAjax)
            return 'Ajax request is not found!';

        if(\Yii::$app->request->post() && isset(\Yii::$app->request->post()['product']) ){
            $post = \Yii::$app->request->post()['product'];
            $product = Product::findOne($post['product_id']);
            if($product) {
                $response = [];
                $response['name'] = $product->name;
                $response['product_id'] = $product->id;
                $response['quantity'] = $post['quantity'];
                $response['period'] = $post['period'];
                $response['price'] = $product->getCurrentPrice(null, $post['period']);
                $response['total_sum'] = $response['quantity'] * $response['price'];

                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $response;
            }
        }
        return 'Empty';
    }

    public function actionTree()
    {
      //  echo "asdasd"; exit;
       // if(!\Yii::$app->request->isAjax)
       //    return 'Ajax request is not found!';
       // else {
          \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return CategoryProduct::find()->asArray()->all();
       // }
    }

    public function actionDelete($id)
    {
        $type = 'product';
        $model = $this->findModel($this->modelName, $id);
        if($model) {
            switch($model->product_type_id){
                case Product::TYPE_PRODUCT: $type = 'product'; break;
                case Product::TYPE_SERVICE: $type = 'service'; break;
            }
        }        
        if($model->delete() !== false)
            $this->setFlash('success', \Yii::t('app', 'Modifications have been saved'));
        else
            $this->setFlash('error', \Yii::t('app', 'Modifications have not been saved'));

        return $this->redirect([($type == 'product' ? 'list' : 'list')]);

    }

    public function actionGenerateCode()
    {
        return $this->render('form', ['model' => $this->createModel($this->modelName),
                                      'barcode' => Product::generateBarcode(),
                                      'listCategories' => array() ]);
    }

    public function actionList(){

        $category_id = \Yii::$app->request->get('category_id');

        $dataProvider = new ActiveDataProvider([
            'query' => Product::find()->productService($category_id),
            'sort' => [
            ],
        ]);

        $categories = CategoryProduct::find()->all();

        return $this->render('list', ['provider' => $dataProvider, 'categories' => $categories]);

    }


}