<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=rental_erp',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
