<?php
namespace app\components\widgets;

use execut\yii\web\AssetBundle;

/**
 * Custom styles
 *
 * @author eXeCUT
 */
class TreeViewAsset extends AssetBundle {
    public $depends = [
        'execut\widget\TreeViewBowerAsset',
        'yii\jui\JuiAsset',
    ];
}