<?php

use yii\db\Migration;

class m161214_123428_product_prices extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product_price}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'pricelist_id' => $this->integer(),
            'price_type_id' => $this->integer(),
            'price' => $this->double(2)
        ], $tableOptions);

        $this->addForeignKey('product_price_pricelist_fk', '{{%product_price}}', 'pricelist_id', '{{%pricelist}}', 'id', 'CASCADE');
        $this->addForeignKey('product_price_pricetype_fk', '{{%product_price}}', 'price_type_id', '{{%price_type}}', 'id', 'CASCADE');
        $this->addForeignKey('product_price_product_fk', '{{%product_price}}', 'product_id', '{{%product}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%product_price}}');
    }

}
