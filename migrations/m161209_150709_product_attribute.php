<?php

use yii\db\Migration;

class m161209_150709_product_attribute extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product_attribute}}', [
            'id' => $this->primaryKey(),
            'attribute_id' => $this->integer(),
            'product_id' => $this->integer(),
            'value' => $this->string(255),
        ], $tableOptions);

        $this->addForeignKey('product_attribute_attribute_fk', '{{%product_attribute}}', 'attribute_id', '{{%attribute}}', 'id', 'RESTRICT');
        $this->addForeignKey('product_attribute_product_fk', '{{%product_attribute}}', 'product_id', '{{%product}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%product_attribute}}');
    }

}
