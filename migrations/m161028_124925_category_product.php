<?php

use yii\db\Migration;

class m161028_124925_category_product extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%category_product}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'parent_id' => $this->integer()->defaultValue(0),
            'fixed' => $this->integer()->defaultValue(0),
            'product_type_id' => $this->integer(),
            'sort' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('idx_parent_id', '{{%category_product}}', 'parent_id');

        $this->insert('{{%category_product}}', ['name' => 'Оборудование', 'parent_id' => 0, 'sort' => 0, 'fixed' => 1]);
        $this->insert('{{%category_product}}', ['name' => 'Внутренние услуги', 'parent_id' => 0, 'sort' => 3, 'fixed' => 1]);
        $this->insert('{{%category_product}}', ['name' => 'Внешние услуги', 'parent_id' => 0, 'fixed' => 1, 'sort' => 4]);

        $this->insert('{{%category_product}}', ['name' => 'Электрооборудование', 'parent_id' => 1, 'sort' => 1]);
        $this->insert('{{%category_product}}', ['name' => 'Музыкальное оборудование', 'parent_id' => 1, 'sort' => 2]);



    }

    public function down()
    {
        $this->dropTable('{{%category_product}}');
    }

}
