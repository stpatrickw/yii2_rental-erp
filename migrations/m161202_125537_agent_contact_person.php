<?php

use yii\db\Migration;

class m161202_125537_agent_contact_person extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%agent_contact}}', [
            'id' => $this->primaryKey(),
            'agent_id' => $this->integer(),
            'first_name' => $this->string(255),
            'last_name' => $this->string(255),
            'middle_name' => $this->string(255),
            'phone' => $this->string(255),
            'email' => $this->string(255),
            'position' => $this->string(255),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%agent_contact}}');
    }

}
