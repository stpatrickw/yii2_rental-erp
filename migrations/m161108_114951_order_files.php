<?php

use yii\db\Migration;

class m161108_114951_order_files extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%order_file}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'name' => $this->string(255),

            'file_path' => $this->string(255),
            'file_base_url' => $this->string(255),
            'priority' => $this->integer()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%order_file}}');
    }


}
