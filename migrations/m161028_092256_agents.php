<?php

use yii\db\Migration;

class m161028_092256_agents extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%agent}}', [
            'id' => $this->primaryKey(),
            'agent_type_id' => $this->integer(),
            'agent_name' => $this->string(255),
            'agent_name_short' => $this->string(255),
            'address_post' => $this->string(255),
            'address_legal' => $this->string(255),
            'inn' => $this->string(255),
            'phone' => $this->string(255),
            'email' => $this->string(255),
            'description' => $this->text(),
            'bank_name' => $this->string(255),
            'bank_rs' => $this->string(255),
            'bank_kors' => $this->string(255),
            'bank_bik' => $this->string(255),
            'bank_oktmo' => $this->string(255),
            'use_vat' => $this->integer()->defaultValue(0),
            'is_supplier' => $this->integer(0)->defaultValue(0),
            'is_customer' => $this->integer(0)->defaultValue(0)
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%agent}}');
    }

}
