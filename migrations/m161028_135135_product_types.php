<?php

use yii\db\Migration;

class m161028_135135_product_types extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'sort' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->insert('{{%product_type}}', [ 'name' => 'Продукт', 'sort' => 0]);
        $this->insert('{{%product_type}}', [ 'name' => 'Услуга', 'sort' => 1]);
    }

    public function down()
    {
        $this->dropTable('{{%product_type}}');
    }

}
