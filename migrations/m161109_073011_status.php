<?php

use yii\db\Migration;

class m161109_073011_status extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'sort' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->insert('{{%status}}', ['name' => 'Планируется', 'sort' => 1]);
        $this->insert('{{%status}}', ['name' => 'В обработке', 'sort' => 2]);
    }

    public function down()
    {
        $this->dropTable('{{%status}}');
    }


}
