<?php

use yii\db\Migration;

class m161212_144435_product_movement extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product_movement}}', [
            'id' => $this->primaryKey(),
            'movement_date' => $this->dateTime(),
            'doc_id' => $this->integer(),
            'product_variant_id' => $this->integer(),
            'quantity' => $this->integer(),
            'warehouse_from_id' => $this->integer(),
            'warehouse_to_id' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-doc-id', '{{%product_movement}}', 'doc_id');
        $this->createIndex('idx-product-variant-id', '{{%product_movement}}', 'product_variant_id');
        $this->createIndex('idx-warehouse-from-id', '{{%product_movement}}', 'warehouse_from_id');
        $this->createIndex('idx-warehouse-to-id', '{{%product_movement}}', 'warehouse_to_id');
    }

    public function down()
    {
        $this->dropTable('{{%product_movement}}');
    }


}
