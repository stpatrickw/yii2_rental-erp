<?php

use yii\db\Migration;

class m161028_141615_product extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'product_type_id' => $this->integer()->defaultValue(0),
            'name' => $this->string(),
            'model' => $this->string(),
            'barcode' => $this->string(),
            'category_id' => $this->integer(),
            'owner_type' => $this->integer()->defaultValue(0),
            'owner_agent_id' => $this->integer(),
            'image_path' => $this->string(255),
            'image_base_url' => $this->string(255),
            'sort' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->addForeignKey('product_category_fk', '{{%product}}', 'category_id', '{{%category_product}}', 'id', 'RESTRICT');
        $this->addForeignKey('product_product_type_fk', '{{%product}}', 'product_type_id', '{{%product_type}}', 'id', 'RESTRICT');

        $this->insert('{{%product}}', [ 'name' => 'Микрофон 243',
                                        'model' => 'MS-243',
                                        'barcode' => '23123332',
                                        'category_id' => 5,
                                        'product_type_id' => 1,
                                        'sort' => 0]);
        $this->insert('{{%product}}', [ 'name' => 'Колонки Sven Royal',
                                        'model' => 'SN-211-22',
                                        'barcode' => '66655443',
                                        'category_id' => 5,
                                        'product_type_id' => 1,
                                        'sort' => 0]);
        $this->insert('{{%product}}', [ 'name' => 'Доставка груза',
                                        'category_id' => 2,
                                        'product_type_id' => 2,
                                        'sort' => 0]);
        $this->insert('{{%product}}', [ 'name' => 'Монтаж оборудования',
                                        'category_id' => 2,
                                        'product_type_id' => 2,
                                        'sort' => 0]);
        $this->insert('{{%product}}', [ 'name' => 'Такси',
                                        'category_id' => 3,
                                        'product_type_id' => 2,
                                        'sort' => 0]);
        $this->insert('{{%product}}', [ 'name' => 'Аренда жилья',
                                        'category_id' => 3,
                                        'product_type_id' => 2,
                                        'sort' => 0]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%product}}');
    }

}
