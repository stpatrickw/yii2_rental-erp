<?php

use yii\db\Migration;

class m161212_142854_documents extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%doc}}', [
            'id' => $this->primaryKey(),
            'doc_type_id' => $this->integer(),
            'warehouse_id' => $this->integer(),
            'user_id' => $this->integer(),
            'agent_id' => $this->integer(),
            // 'event_id' => $this->integer(),
            'event_name' => $this->string(500),
            'date_start' => $this->dateTime(),
            'date_end' => $this->dateTime(),
            'status_id' => $this->integer()->defaultValue(0),
            'priority' => $this->integer()->defaultValue(0),
            'period' => $this->integer()->defaultValue(0),

            'discount' => $this->double(2),
            'total_sum' => $this->double(2),

            'doc_date' => $this->date(),
            'is_dry' => $this->integer()->defaultValue(0),
            'description' => $this->text(),
            'created_date' => $this->timestamp(),
            'updated_date' => $this->timestamp(),
            'event_start_date' => $this->date(),
            'event_end_date' => $this->date(),
            'reserve_start_date' => $this->dateTime(),
            'reserve_end_date' => $this->dateTime(),

        ], $tableOptions);

        $this->createIndex('idx-doc-type-id', '{{%doc}}', 'doc_type_id');
        $this->createIndex('idx-agent-id', '{{%doc}}', 'agent_id');
        $this->createIndex('idx-warehouse-id', '{{%doc}}', 'warehouse_id');
        $this->createIndex('idx-user-id', '{{%doc}}', 'user_id');
        $this->createIndex('idx-status-id', '{{%doc}}', 'status_id');

    }

    public function down()
    {
        $this->dropTable('{{%doc}}');
    }

}
