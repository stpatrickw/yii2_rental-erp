<?php

use yii\db\Migration;

class m161212_145137_product_stock extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product_stock}}', [
            'id' => $this->primaryKey(),
            'warehouse_id' => $this->integer(),
            'product_variant_id' => $this->integer(),
            'quantity' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-product-variant-id', '{{%product_stock}}', 'product_variant_id');
        $this->createIndex('idx-warehouse-id', '{{%product_stock}}', 'warehouse_id');
    }

    public function down()
    {
        $this->dropTable('{{%product_stock}}');
    }

}
