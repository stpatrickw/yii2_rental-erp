<?php

use yii\db\Migration;

class m161213_141235_pricelists extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%pricelist}}', [
            'id' => $this->primaryKey(),
            'pricelist_date' => $this->date(),
            'user_id_created' => $this->integer(),
            'created_date' => $this->timestamp(),
            'updated_date' => $this->timestamp(),
            'description' => $this->text(),
        ], $tableOptions);

        $this->createIndex('idx-user-id-created', '{{%pricelist}}', 'user_id_created');
        $this->createIndex('idx-price-date', '{{%pricelist}}', 'pricelist_date');

    }

    public function safeDown()
    {
        $this->dropTable('{{%pricelist}}');
    }

}
