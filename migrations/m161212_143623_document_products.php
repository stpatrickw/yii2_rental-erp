<?php

use yii\db\Migration;

class m161212_143623_document_products extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%doc_product}}', [
            'id' => $this->primaryKey(),
            'doc_id' => $this->integer(),
            'agent_id' => $this->integer(),
            'product_id' => $this->integer(),
            'period' => $this->integer(),
            'quantity' => $this->integer(),
            'vat' => $this->double(2),
            'price' => $this->double(2),
            'price_supplier' => $this->double(2),
            'total_sum' => $this->double(2),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%doc_product}}');
    }

}
