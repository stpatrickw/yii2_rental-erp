<?php

use yii\db\Migration;

class m161028_144315_warehouses extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%warehouse}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'sort' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->insert('{{%warehouse}}', ['name' => 'Склад большой']);
        $this->insert('{{%warehouse}}', ['name' => 'Склад маленький']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%warehouse}}');
    }

}
