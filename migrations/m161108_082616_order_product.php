<?php

use yii\db\Migration;

class m161108_082616_order_product extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%order_product}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'agent_id' => $this->integer(),
            'type' => $this->integer(), // product = 1. internal = 2, external = 3
            'product_id' => $this->integer(),
            'period' => $this->integer(),
            'quantity' => $this->integer(),
            'vat' => $this->double(2),
            'price' => $this->double(2),
            'price_supplier' => $this->double(2),
            'total_sum' => $this->double(2),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%order_product}}');
    }

}
