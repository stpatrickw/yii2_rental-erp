<?php

use yii\db\Migration;

class m161208_105011_attributes_ref extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%attribute}}', [
            'id' => $this->primaryKey(),
            'attribute_group_id' => $this->integer()->defaultValue(0),
            'name' => $this->string(),
            'sort' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('idx-attribute-group', '{{%attribute}}','attribute_group_id');

    }

    public function safeDown()
    {
        $this->dropTable('{{%attribute}}');
    }

}
