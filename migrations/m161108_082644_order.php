<?php

use yii\db\Migration;

class m161108_082644_order extends Migration
{
    public function up()
    {

            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

            $this->createTable('{{%order}}', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer(),
                'agent_id' => $this->integer(),
               // 'event_id' => $this->integer(),
                'event_name' => $this->string(500),
                'date_start' => $this->dateTime(),
                'date_end' => $this->dateTime(),
                'is_estimate' => $this->integer()->defaultValue(0),
                'status_id' => $this->integer()->defaultValue(0),
                'priority' => $this->integer()->defaultValue(0),

                'discount' => $this->double(2),
                'total_sum' => $this->double(2),

                'order_date' => $this->date(),
                'created_date' => $this->timestamp(),
                'updated_date' => $this->timestamp(),
                'event_start_date' => $this->date(),
                'event_end_date' => $this->date(),
                'reserve_start_date' => $this->dateTime(),
                'reserve_end_date' => $this->dateTime(),

            ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%order}}');
    }


}
