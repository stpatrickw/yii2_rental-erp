<?php

use yii\db\Migration;

class m161212_143309_document_files extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%doc_file}}', [
            'id' => $this->primaryKey(),
            'doc_id' => $this->integer(),
            'name' => $this->string(255),

            'file_path' => $this->string(255),
            'file_base_url' => $this->string(255),
            'priority' => $this->integer(),
            'sort' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
       $this->dropTable('{{%doc_file}}');
    }

}
