<?php

use yii\db\Migration;

class m161212_104758_product_variants extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product_variant}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'barcode' => $this->string(13),
            'serial' => $this->string(255),
        ], $tableOptions);

        $this->addForeignKey('product_variant_product_fk', '{{%product_variant}}', 'product_id', '{{%product}}', 'id', 'CASCADE');
        $this->createIndex('idx_barcode', '{{%product_variant}}', 'barcode');
        $this->createIndex('idx_serial', '{{%product_variant}}', 'serial');
    }

    public function safeDown()
    {
        $this->dropTable('{{%product_variant}}');
    }


}
