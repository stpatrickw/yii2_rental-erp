<?php

use yii\db\Migration;

class m161110_092738_price_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%price_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'is_dry' => $this->integer()->defaultValue(0),
            'for_product' => $this->integer()->defaultValue(0),
            'for_service' => $this->integer()->defaultValue(0),
            'use_range' => $this->integer()->defaultValue(0),
            'range_min' => $this->integer(),
            'range_max' => $this->integer(),
            'sort' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->insert('{{%price_type}}', ['name' => 'Цена 1 дня', 'for_product' => 1, 'for_service' => 1, 'is_dry' => 0, 'use_range' => 1, 'range_min' => 1, 'range_max' => 1,  'sort' => 3]);
        $this->insert('{{%price_type}}', ['name' => 'Цена 2 дней', 'for_product' => 1, 'for_service' => 0, 'is_dry' => 0, 'use_range' => 1, 'range_min' => 2, 'range_max' => 2,  'sort' => 4]);
        $this->insert('{{%price_type}}', ['name' => 'Цена 3 дней', 'for_product' => 1, 'for_service' => 0, 'is_dry' => 0, 'use_range' => 1, 'range_min' => 3, 'range_max' => 3,  'sort' => 5]);
        $this->insert('{{%price_type}}', ['name' => 'Цена 4 дней', 'for_product' => 1, 'for_service' => 0, 'is_dry' => 0, 'use_range' => 1, 'range_min' => 4, 'range_max' => 4,  'sort' => 6]);
        $this->insert('{{%price_type}}', ['name' => 'Цена 5 дней и более', 'for_product' => 1, 'for_service' => 0, 'is_dry' => 0, 'use_range' => 1, 'range_min' => 5, 'range_max' => 999,  'sort' => 7]);
        $this->insert('{{%price_type}}', ['name' => 'Цена 1 дня', 'for_product' => 1, 'for_service' => 0, 'is_dry' => 1, 'use_range' => 1, 'range_min' => 1, 'range_max' => 1,  'sort' => 8]);
        $this->insert('{{%price_type}}', ['name' => 'Цена 2 дней', 'for_product' => 1, 'for_service' => 0, 'is_dry' => 1, 'use_range' => 1, 'range_min' => 2, 'range_max' => 2,  'sort' => 9]);
        $this->insert('{{%price_type}}', ['name' => 'Цена 3 дней', 'for_product' => 1, 'for_service' => 0, 'is_dry' => 1, 'use_range' => 1, 'range_min' => 3, 'range_max' => 3,  'sort' => 10]);
        $this->insert('{{%price_type}}', ['name' => 'Цена 4 дней', 'for_product' => 1, 'for_service' => 0, 'is_dry' => 1, 'use_range' => 1, 'range_min' => 4, 'range_max' => 4,  'sort' => 11]);
        $this->insert('{{%price_type}}', ['name' => 'Цена 5 дней и более', 'for_product' => 1, 'for_service' => 0, 'is_dry' => 1, 'use_range' => 1, 'range_min' => 5, 'range_max' => 999,  'sort' => 12]);
    }

    public function down()
    {
        $this->dropTable('{{%price_type}}');
    }

}
