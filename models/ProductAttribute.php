<?php

namespace app\models;

use Yii;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $text
 * @property integer $sort
 */
class ProductAttribute extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'attribute_id'], 'integer'],
            ['attribute_id', 'required'],
            ['value', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('app', 'Product'),
            'attribute_id' => Yii::t('app', 'Attribute'),
            'value' => Yii::t('app', 'Value'),

        ];
    }

    public function getProduct(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getProductAttribute(){
        return $this->hasOne(Attribute::className(), ['id' => 'attribute_id']);
    }


}
