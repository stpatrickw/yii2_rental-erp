<?php

namespace app\models;

use Yii;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $text
 * @property integer $sort
 */
class PriceType extends \yii\db\ActiveRecord
{

    const STATUS_ON = 1;
    const STATUS_OFF = 0;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string', 'max' => 255],
            ['description', 'string'],
            [['sort', 'use_range', 'range_min', 'range_max', 'is_dry', 'for_product', 'for_service'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'use_range' => Yii::t('app', 'Use range period in days'),
            'for_product' => Yii::t('app', 'For product'),
            'for_service' => Yii::t('app', 'For service'),
            'range_min' => Yii::t('app', 'Range min'),
            'range_max' => Yii::t('app', 'Range max'),
            'is_dry' => Yii::t('app', 'Dry price'),
            'sort' => Yii::t('app', 'Sort'),

        ];
    }

    public function getProvider()
    {

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => self::find(),
            'sort' => [
                'defaultOrder' => ['id' => SORT_ASC]
            ],
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    public static function getStatuses(){
        return [self::STATUS_OFF => Yii::t('app', 'No'),
                self::STATUS_ON => Yii::t('app', 'Yes')];

    }

    public function getUseRangeStatus(){
        return self::getStatuses()[$this->use_range];
    }

    public function getDryStatus(){
        return self::getStatuses()[$this->is_dry];
    }

    public function getforProductStatus(){
        return self::getStatuses()[$this->for_product];
    }
    public function getforServiceStatus(){
        return self::getStatuses()[$this->for_service];
    }

}
