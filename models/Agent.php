<?php

namespace app\models;

use app\models\query\AgentQuery;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $text
 * @property integer $sort
 */
class Agent extends \yii\db\ActiveRecord
{
use \app\components\GetProvider;

    const TYPE_CUSTOMER = 1;
    const TYPE_SUPPLIER = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['inn', 'unique', 'targetClass' => Agent::className(), 'targetAttribute' => 'inn', 'message'=> \Yii::t('app', 'Inn already exist')],
            [[  'agent_name',
                'agent_name_short',
                'address_post',
                'address_legal',
                'inn',
                'phone',
                'email',
                'bank_name',
                'bank_rs',
                'bank_kors',
                'bank_bik',
                'bank_oktmo'], 'string', 'max' => 255],
            ['description', 'string'],
            [['agent_name'], 'required'],
            [['use_vat', 'is_supplier', 'is_customer'], 'integer'],
            // ['agent_type_id', 'integer'],
            ['email', 'email']
        ];
    }

    public function checkAgentType($attribute, $params)
    {
        if(!$this->is_supplier && !$this->is_customer){
            $this->addError($attribute, Yii::t('app', 'Agent must have at least one type'));
        }

        return true;
    }


    /**
     * @inheritdoc
     */
    public function search($params){

        $query = Agent::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        //For validation uncomment
        /* if ($this->validate()) {
            return $dataProvider;
        }
        */

        $query->andFilterWhere(['like', 'agent_name', $this->agent_name]);
        $query->andFilterWhere(['like', 'agent_name_short', $this->agent_name_short]);
        $query->andFilterWhere(['like', 'phone', $this->phone]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['is_customer' => $this->is_customer]);
        $query->andFilterWhere(['is_supplier' => $this->is_supplier]);


        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'agent_name' => Yii::t('app', 'Agent name'),
            'agent_name_short' => Yii::t('app', 'Agent name short'),
            'address_post' => Yii::t('app', 'Address post'),
            'address_legal' => Yii::t('app', 'Address legal'),
            'inn' => Yii::t('app', 'Inn'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'bank_name' => Yii::t('app', 'Bank name'),
            'bank_rs' => Yii::t('app', 'Bank rs'),
            'bank_kors' => Yii::t('app', 'Bank kors'),
            'bank_bik' => Yii::t('app', 'Bank bik'),
            'bank_oktmo' => Yii::t('app', 'Bank oktmo'),
            'agent_type_id' => Yii::t('app', 'Agent type'),
            'description' => Yii::t('app', 'Description'),
            'is_supplier' => Yii::t('app', 'Supplier'),
            'is_customer' => Yii::t('app', 'Customer'),
            'use_vat' => Yii::t('app', 'Use vat'),

        ];
    }

    public static function find()
    {
        return new AgentQuery(get_called_class());
    }

    public static function getTypes()
    {
        return array( self::TYPE_CUSTOMER => Yii::t('app', 'Customer'), self::TYPE_SUPPLIER => Yii::t('app', 'Supplier') );
    }


    public function getDebt()
    {
        return 0;
    }

    public function getContacts()
    {
        return $this->hasMany(AgentContact::className(), ['agent_id' => 'id']);
    }

    public function deleteContacts(){
        AgentContact::deleteAll(['agent_id' => $this->id]);
    }


}
