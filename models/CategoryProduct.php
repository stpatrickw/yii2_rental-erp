<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $text
 * @property integer $sort
 */
class CategoryProduct extends \yii\db\ActiveRecord
{
use \app\components\GetProvider;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string'],
            ['parent_id', 'integer'],
            ['parent_id', 'default', 'value' => 0],
            ['sort', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'parent_id' => Yii::t('app', 'Parent category'),
            'sort' => Yii::t('app', 'Sort'),
            'fullName' => Yii::t('app', 'Name'),
        ];
    }

    public function getParent()
    {
        return $this->hasOne(CategoryProduct::className(), ['id' => 'parent_id']);
    }

    public function getFullName()
    {
        $cats = array();
        $model_resursive = $this;
        while($model_resursive->parent){
            $cats[] = $model_resursive->parent->name;
            $model_resursive = $model_resursive->parent;
        }
        $cats = array_reverse($cats);
        $cats[] = $this->name;
        return implode(' \ ', $cats);
    }

    public static function getChidsArray($id = 0)
    {
        $arr = [];
        foreach (self::find()->where(['parent_id' => $id])->orderBy('name')->all() as $child){
            $sub_childs = self::getChidsArray($child->id);
            $child_arr['text'] = $child->name;
            $child_arr['href'] = Url::to(['product/list', 'category_id' => $child->id]);
            $child_arr['category_id'] = $child->id;

            if($sub_childs) $child_arr['nodes'] = $sub_childs;
            $arr[] = $child_arr;
        }

        return $arr;
    }

    public static function getChidsArrayShort($id = 0)
    {
        $arr = [];
        foreach (self::find()->where(['parent_id' => $id])->orderBy('name')->all() as $child){
            $sub_childs = self::getChidsArrayShort($child->id);

            if($sub_childs) $child_arr[$child->name] = $sub_childs;
                else    $child_arr[$child->id] = $child->name;

            $arr = $child_arr;
        }

        return $arr;
    }

    public static function getChidsIdArray($category_id = 0, &$arr)
    {
        foreach (self::find()->where(['parent_id' => $category_id])->all() as $child){
            $sub_childs = self::getChidsIdArray($child->id, $arr);
            array_push($arr, $child->id);
        }
    }

    public static function getCategoryAndChildIds($category_id = 0){
        $arr = [];
        array_push($arr, $category_id);
        self::getChidsIdArray($category_id, $arr);

        return $arr;
    }


}
