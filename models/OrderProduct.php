<?php

namespace app\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\TimestampBehavior;
use app\models\OrderFile;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $text
 * @property integer $sort
 */
class OrderProduct extends \yii\db\ActiveRecord
{
use \app\components\GetProvider;

    public $files;
    const IS_ESTIMATE = 1;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[  'order_id',
                'product_id',
                'type',
                'period',
                'quantity', 'agent_id' ], 'integer'],
            [['price', 'price_supplier', 'vat', 'total_sum'], 'double']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('app', 'Order/Estimate'),
            'product_id' => Yii::t('app', 'Product'),
            'agent_id' => Yii::t('app', 'Supplier'),
            'period' => Yii::t('app', 'Period'),
            'quantity' => Yii::t('app', 'Quantity'),
            'price' => Yii::t('app', 'Price'),
            'price_supplier' => Yii::t('app', 'Price supplier'),
            'vat' => Yii::t('app', 'Vat'),
            'type' => Yii::t('app', 'Type'),
            'debt' => Yii::t('app', 'Debt'),
            'total_sum' => Yii::t('app', 'Total sum'),
        ];
    }

    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    public function getAgent()
    {
        return $this->hasOne(Agent::className(), ['id' => 'agent_id']);
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }



}
