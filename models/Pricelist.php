<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $text
 * @property integer $sort
 */
class Pricelist extends \yii\db\ActiveRecord
{
use \app\components\GetProvider;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => 'updated_date',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id_created'], 'integer'],
            ['description', 'string'],
            [['pricelist_date'], 'date', 'format' => 'yyyy-M-d' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Id'),
            'user_id_created' => Yii::t('app', 'User created'),
            'description' => Yii::t('app', 'Description'),
            'productCount' => Yii::t('app', 'Item count'),
            'pricelist_date' => Yii::t('app', 'Pricelist date'),
            'userName' => Yii::t('app', 'User'),
        ];
    }

    public function beforeSave($insert)
    {
        if($insert)
            $this->user_id_created = \Yii::$app->user->id;
        return parent::beforeSave($insert);
    }

    public function getProductPrices()
    {
        return $this->hasMany(ProductPrice::className(), ['pricelist_id' => 'id']);
    }

    public function getProductCount()
    {
        return $this->getProductPrices()->groupBy(['product_id'])->count();
    }

    public function deletePrices(){
        ProductPrice::deleteAll(['pricelist_id' => $this->id]);
    }

    public function getUserName()
    {
        return (isset($this->user) ? $this->user->last_name : '');
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id_created']);
    }

}
