<?php

namespace app\models;

use Yii;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $text
 * @property integer $sort
 */
class AgentContact extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'middle_name', 'position', 'phone', 'email'], 'string', 'max' => 255],
            [['first_name', 'last_name'], 'required'],
            ['agent_id', 'integer'],
            ['email', 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'first_name' => Yii::t('app', 'First name'),
            'last_name' => Yii::t('app', 'Last name'),
            'middle_name' => Yii::t('app', 'Middle name'),
            'position' => Yii::t('app', 'Agent position'),
            'phone' => Yii::t('app', 'Phone main'),
            'email' => Yii::t('app', 'Email'),
            'agent_id' => Yii::t('app', 'Agent'),
        ];
    }

    public function getAgent()
    {
       return $this->hasOne(Agent::className(), ['id' => 'agent_id']);
    }

    public function getFullName(){
        return $this->last_name . ' ' . $this->first_name;
    }

}
