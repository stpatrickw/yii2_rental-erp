<?php

namespace app\models;

use Yii;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $text
 * @property integer $sort
 */
class ProductPrice extends \yii\db\ActiveRecord
{

    public $product_price_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pricelist_id', 'price_type_id'], 'integer'],
            ['price', 'double'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pricelist_id' => Yii::t('app', 'Pricelist'),
            'product_id' => Yii::t('app', 'Product'),
            'price_type_id' => Yii::t('app', 'Price type'),
            'price' => Yii::t('app', 'Price'),

        ];
    }

    public function getProduct(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getProduct_price_date(){
        $pricelist = Pricelist::find()->where(['id' => $this->pricelist_id])->one();
        return $pricelist->pricelist_date;
    }

    public function getPricelist(){
        return $this->hasOne(Pricelist::className(), ['id' => 'pricelist_id']);
    }

}
