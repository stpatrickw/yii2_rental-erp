<?php

namespace app\models;

use app\models\query\ProductQuery;
use Yii;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $text
 * @property integer $sort
 */
class Product extends \yii\db\ActiveRecord
{
use \app\components\GetProvider;

    public $image;

    const TYPE_PRODUCT  = 1;
    const TYPE_SERVICE = 2;

    const OWNER_TYPE_INTERNAL = 0;
    const OWNER_TYPE_EXTERNAL = 1;


    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image',
                'pathAttribute' => 'image_path',
                'baseUrlAttribute' => 'image_base_url',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'model', 'barcode' ], 'string'],
            [['category_id', 'product_type_id', 'sort', 'owner_type', 'owner_agent_id'], 'integer'],
            ['name', 'required'],
            ['image', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'model' => Yii::t('app', 'Model'),
            'product_type_id' => Yii::t('app', 'Product type'),
            'category_id' => Yii::t('app', 'Product category'),
            'owner_type' => Yii::t('app', 'Owner type'),
            'owner_agent_id' => Yii::t('app', 'Owner agent'),
            'barcode' => Yii::t('app', 'Barcode'),
            'categoryName' => Yii::t('app', 'Product category'),
            'quantity' => Yii::t('app', 'Quantity'),
            'image' => Yii::t('app', 'Image'),
            'sort' => Yii::t('app', 'Sort'),
        ];
    }

    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    public function getMainImageUrl()
    {
        return Yii::$app->request->hostInfo . $this->image_base_url . '/' . $this->image_path;
    }

    public function getCategoryName(){
        $category = CategoryProduct::find()->where(['id' => $this->category_id])->one();

        return $category ? $category->name : null;
    }

    public function getCategory(){
        $this->hasOne(CategoryProduct::className(), ['category_id' => 'id']);
    }

    public static function generateBarcode(){
        return rand(1000000,9999999);
    }

   /* public function getPrices()
    {
        return $this->hasMany(ProductPrice::className(), ['product_id' => 'id'])->orderBy('product_price_date');
    }
*/
    public function getCurrentPrice($price_type_id = null, $period = null){

        if($period) {
            $price_type = PriceType::find()->where(['>=', 'range_max', $period])->andWhere(['<=', 'range_min', $period])->one();
            if($price_type) {
                $price_type_id = $price_type->id;
            }
        }
        if($price_type_id){
            return $this->getLastPrice($price_type_id);
        }

        return 0;
    }


    public function deleteAttributes(){
        ProductAttribute::deleteAll(['product_id' => $this->id]);
    }

    public function deleteVariants(){
        ProductVariant::deleteAll(['product_id' => $this->id]);
    }

    public function getQuantity(){
        return ProductQuantity::find()->where(['product_id'=> $this->id])->sum('quantity');
    }


    public function getProductAttributes(){
        return $this->hasMany(ProductAttribute::className(), ['product_id' => 'id']);
    }

    public function getProductVariants(){
        return $this->hasMany(ProductVariant::className(), ['product_id' => 'id']);
    }


    public function getLastPrice($price_type_id = 0){
        $priceModel = ProductPrice::find()->leftJoin(Pricelist::tableName(), Pricelist::tableName().'.id = '. ProductPrice::tableName(). '.pricelist_id')->where(['product_id' => $this->id, 'price_type_id' => $price_type_id])->orderBy(Pricelist::tableName().'.pricelist_date DESC, '. Pricelist::tableName().'.id DESC')->limit(1)->one();

        return ($priceModel ? $priceModel->price : '');
    }

    public function getLastPrices(){
        $priceTypes = PriceType::find()->orderBy('sort')->all();

        $prices = [];
        foreach ($priceTypes as $priceType){
            $prices[$priceType->id] = $this->getLastPrice($priceType->id);
        }

        return $prices;
    }

    public function afterDelete()
    {
        $this->deleteAttributes();
        $this->deleteVariants();
        parent::afterDelete();
    }

}
