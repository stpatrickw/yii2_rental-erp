<?php

namespace app\models;

use Yii;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $name
 * @property integer attribut_group_id
 * @property integer $sort
 */
class Attribute extends \yii\db\ActiveRecord
{

    use \app\components\GetProvider;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string', 'max' => 255],
            [['sort', 'attribute_group_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'attributeGroup' => Yii::t('app', 'Attribute group'),
            'sort' => Yii::t('app', 'Sort'),
        ];
    }

    public function getAttributeGroup()
    {
        return $this->hasOne(AttributeGroup::className(), ['id' => 'attribute_group_id']);
    }

    public static function getAttributesArrayForSelect()
    {
        $arr = array();
        foreach (self::find()->joinWith('attributeGroup ag')->orderBy(['ag.name' => SORT_ASC, 'name' => SORT_ASC])->all() as $item){
            $arr[$item->id] = (isset($item->attributeGroup) ? $item->attributeGroup->name. ': ' : '') . $item->name;
        }
        return $arr;
    }

}
