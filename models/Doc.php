<?php

namespace app\models;

use app\models\query\DocQuery;
use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $text
 * @property integer $sort
 */
class Doc extends \yii\db\ActiveRecord
{

    public $files;
  //  public $agentName;
    const DOC_TYPE_RECEIVE = 1;
    const DOC_TYPE_ESTIMATE = 2;


    public function scenarios()
    {
        $scenarios = parent::scenarios();
       // $scenarios[self::DOC_TYPE_ESTIMATE] = [];
        return $scenarios;
    }

    public function setDocType($id_doc_type = 0, $scenario = 0){
        $this->doc_type_id = $id_doc_type;
        if($scenario) $this->scenario = $scenario;
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => 'updated_date',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'files',
                'multiple' => true,
                'uploadRelation' => 'docFiles',
                'pathAttribute' => 'file_path',
                'baseUrlAttribute' => 'file_base_url',
                'orderAttribute' => 'sort',
                //  'typeAttribute' => 'type',
                //  'sizeAttribute' => 'size',
                //  'nameAttribute' => 'name',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[  'id',
                'user_id',
                'agent_id',
                'doc_type_id',
                'status_id',
                'priority',
                'period' ], 'integer'],
            ['event_name', 'string'],
            [['discount', 'total_sum'], 'double'],
            [['event_start_date', 'event_end_date'], 'date', 'format' => 'yyyy-M-d' ],
            ['doc_date', 'date', 'format' => 'yyyy-M-d'],
            ['files', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Id'),
            'user_id' => Yii::t('app', 'User'),
            'agent_id' => Yii::t('app', 'Agent'),
            'agent_name' => Yii::t('app', 'Agent name'),
            'agentName' => Yii::t('app', 'Agent name'),
            'event_name' => Yii::t('app', 'Event name'),
            'doc_type_id' => Yii::t('app', 'Document type'),
            'status_id' => Yii::t('app', 'Status'),
            'priority' => Yii::t('app', 'Priority'),
            'period' => Yii::t('app', 'Period'),
            'files' => Yii::t('app', 'Files'),
            'discount' => Yii::t('app', 'Discount'),
            'total_sum' => Yii::t('app', 'Total sum'),
            'doc_date' => Yii::t('app', 'Order date'),
            'date_start' => Yii::t('app', 'Date start'),
            'date_end' => Yii::t('app', 'Date end'),
            'event_start_date' => Yii::t('app', 'Event start date'),
            'event_end_date' => Yii::t('app', 'Event end date'),
            'reserve_start_date' => Yii::t('app', 'Reserve start date'),
            'reserve_end_date' => Yii::t('app', 'Reserve end date'),
            'companyFullName' => Yii::t('app', 'Company full name'),
        ];
    }

    public static function find()
    {
        return new DocQuery(get_called_class());
    }

    public function getDocFiles()
    {
        return $this->hasMany(DocFile::className(), ['doc_id' => 'id']);
    }

    public function getProducts()
    {
        return $this->hasMany(DocProduct::className(), ['doc_id' => 'id']);
    }

    public function getAgent()
    {
        return $this->hasOne(Agent::className(), ['id' => 'agent_id']);
    }

    public function getAgentName()
    {
        $agent = Agent::find()->where(['id' => $this->agent_id])->one();

        return ($agent ? $agent->agent_name : '');
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    public function deleteProducts(){
        DocProduct::deleteAll(['doc_id' => $this->id]);
    }

    public function getMaxOrderProductId()
    {
        return DocProduct::find()->max('id') + 1;
    }

    public static function getProvider($doc_type_id = 0)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => self::find()->doc_type($doc_type_id),
            'sort' => [
            ],
        ]);

        return $dataProvider;
    }

}
