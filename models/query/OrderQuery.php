<?php

namespace app\models\query;

use yii\db\ActiveQuery;

class OrderQuery extends ActiveQuery
{

    public function estimate()
    {
        return $this->andWhere(['is_estimate' => 1]);
    }

}