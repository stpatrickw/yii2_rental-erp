<?php

namespace app\models\query;

use yii\db\ActiveQuery;
use app\models\Agent;

class AgentQuery extends ActiveQuery
{

    public function customers()
    {
        return $this->andWhere(['is_customer' => Agent::TYPE_CUSTOMER]);
    }

    public function suppliers()
    {
        return $this->andWhere(['is_supplier' => Agent::TYPE_SUPPLIER]);
    }


}