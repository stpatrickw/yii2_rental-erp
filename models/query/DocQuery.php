<?php

namespace app\models\query;

use yii\db\ActiveQuery;

class DocQuery extends ActiveQuery
{

    public function doc_type($doc_type_id)
    {
        return $this->andWhere(['doc_type_id' => $doc_type_id]);
    }

}