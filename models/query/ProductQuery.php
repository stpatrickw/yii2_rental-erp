<?php

namespace app\models\query;

use app\models\CategoryProduct;
use app\models\Product;
use yii\db\ActiveQuery;

class ProductQuery extends ActiveQuery
{

    public function service($category_id = null)
    {
        if($category_id)
            $this->andWhere(['category_id' => CategoryProduct::getCategoryAndChildIds($category_id)]);
        return $this->andWhere(['product_type_id' => Product::TYPE_SERVICE]);
    }

    public function product($category_id = null)
    {
        if($category_id)
            $this->andWhere(['category_id' => CategoryProduct::getCategoryAndChildIds($category_id)]);
        return $this->andWhere(['product_type_id' => Product::TYPE_PRODUCT]);
    }

    public function productService($category_id = null)
    {
        if($category_id)
            $this->andWhere(['category_id' => CategoryProduct::getCategoryAndChildIds($category_id)]);
        return $this;
    }


}