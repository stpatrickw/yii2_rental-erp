<?php

namespace app\models;

use app\models\query\OrderQuery;
use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\TimestampBehavior;
use app\models\OrderFile;
use yii\data\ActiveDataProvider;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $text
 * @property integer $sort
 */
class Order extends \yii\db\ActiveRecord
{


    public $files;
    const SCENARIO_ORDER = 0;
    const SCENARIO_ESTIMATE = 1;


    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ORDER] = ['username', 'password'];
        $scenarios[self::SCENARIO_ESTIMATE] = ['username', 'email', 'password'];
        return $scenarios;
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => 'updated_date',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'files',
                'multiple' => true,
                'uploadRelation' => 'orderFiles',
                'pathAttribute' => 'file_path',
                'baseUrlAttribute' => 'file_base_url',
                'orderAttribute' => 'sort',
                //  'typeAttribute' => 'type',
                //  'sizeAttribute' => 'size',
                //  'nameAttribute' => 'name',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[  'user_id',
                'agent_id',
              //  'event_id',
                'is_estimate',
                'status_id',
                'priority',
                'period' ], 'integer'],
            //['event_name', 'string', 'on' => self::SCENARIO_ESTIMATE],
            ['event_name', 'string'],
            [['discount', 'total_sum'], 'double'],
            //[['event_start_date', 'event_end_date'], 'datetime', 'format' => 'yyyy-M-d hh:mm', 'on' => self::SCENARIO_ORDER ],
            //[['event_start_date', 'event_end_date'], 'date', 'format' => 'yyyy-M-d', 'on' => self::SCENARIO_ESTIMATE ],
            [['event_start_date', 'event_end_date'], 'date', 'format' => 'yyyy-M-d' ],
            //[['date_start', 'date_end', 'event_start_date', 'event_end_date', 'reserve_start_date', 'reserve_end_date'], 'datetime', 'format' => 'yyyy-M-d hh:mm', 'on' => self::SCENARIO_ORDER ],
            ['order_date', 'date', 'format' => 'yyyy-M-d'],
            ['files', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Id'),
            'user_id' => Yii::t('app', 'User'),
            'agent_id' => Yii::t('app', 'Agent'),
           // 'event_id' => Yii::t('app', 'Event'),
            'event_name' => Yii::t('app', 'Event name'),
            'is_estimate' => Yii::t('app', 'Is estimate'),
            'status_id' => Yii::t('app', 'Status'),
            'priority' => Yii::t('app', 'Priority'),
            'period' => Yii::t('app', 'Period'),
            'files' => Yii::t('app', 'Files'),
            'discount' => Yii::t('app', 'Discount'),
            'total_sum' => Yii::t('app', 'Total sum'),
            'order_date' => Yii::t('app', 'Order date'),
            'date_start' => Yii::t('app', 'Date start'),
            'date_end' => Yii::t('app', 'Date end'),
            'event_start_date' => Yii::t('app', 'Event start date'),
            'event_end_date' => Yii::t('app', 'Event end date'),
            'reserve_start_date' => Yii::t('app', 'Reserve start date'),
            'reserve_end_date' => Yii::t('app', 'Reserve end date'),
            'companyFullName' => Yii::t('app', 'Company full name'),
        ];
    }

    public static function find()
    {
        return new OrderQuery(get_called_class());
    }

    public function getOrderFiles()
    {
        return $this->hasMany(OrderFile::className(), ['order_id' => 'id']);
    }

    public function getProducts()
    {
        return $this->hasMany(OrderProduct::className(), ['order_id' => 'id']);
    }

    public function getAgent()
    {
        return $this->hasOne(Agent::className(), ['id' => 'agent_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    public function deleteProducts(){
        OrderProduct::deleteAll(['order_id' => $this->id]);
    }

    public function getMaxOrderProductId()
    {
        return OrderProduct::find()->max('id') + 1;
    }

    public function getProvider()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => self::find(),
            'sort' => [
            ],
        ]);

        return $dataProvider;
    }

}
