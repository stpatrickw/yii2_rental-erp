<?php

namespace app\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use yii\data\ActiveDataProvider;

/**
 * This is the model class settings
 *
 * @property integer $id
 * @property string $text
 * @property integer $sort
 */
class OrderFile extends \yii\db\ActiveRecord
{

    public $file;

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'file_path',
                'baseUrlAttribute' => 'file_base_url',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name',], 'string'],
            ['file', 'safe'],
            ['sort', 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('admin', 'Name'),
            'file' => Yii::t('admin', 'File'),
            'sort' => Yii::t('admin', 'Sort'),
        ];
    }


    public function getMainFileUrl()
    {
        return \Yii::$app->request->hostInfo . $this->file_base_url . '/' . $this->file_path;
    }

    public function getMainFile()
    {
        if($this->file_base_url && $this->file_path)
            $file = \Yii::getAlias('@app') . $this->file_base_url . '/' . $this->file_path;
        else $file = null;
        return file_exists($file) ? $file : null;
    }

    public function getProvider($camp_id = 1)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => self::find()->where(['camp_id' => $camp_id]),
            'sort' => [],
        ]);

        return $dataProvider;
    }

}
