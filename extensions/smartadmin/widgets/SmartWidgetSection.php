<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\extensions\smartadmin\widgets;

use \yii\base\Widget;
use yii\helpers\Html;

class SmartWidgetSection extends Widget
{

    /**
     * Initializes the widgets section.
     */
    public function init()
    {
        parent::init();
        ob_start();
    }

    public function run()
    {
        $content = '<section id="widget-grid" class="">';
        $content .= ob_get_clean();
        $content .= '</section>';
        return $content;
    }

}
