<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\extensions\smartadmin\widgets;

use \yii\base\Widget;
use yii\helpers\Html;

class SmartWidget extends Widget
{

    public $options = [];

    public $editbutton = true;
    public $colorbutton = true;
    public $togglebutton = true;
    public $deletebutton = true;
    public $fullscreenbutton = true;
    public $custombutton = true;
    public $collapsed = true;
    public $sortable = true;

    // jarviswidget-color-darken
    public $theme = 'jarviswidget-color-purple';

    public $icon;
    public $title = 'Widget';


    /**
     * Initializes the widgets section.
     */
    public function init()
    {
        parent::init();
        ob_start();
    }

    public function run()
    {
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        $this->options['class'] = 'jarviswidget';
        Html::addCssClass($this->options, $this->theme);
        if(!$this->editbutton) $this->options['data-widget-editbutton'] = 'false';
        if(!$this->colorbutton) $this->options['data-widget-colorbutton'] = 'false';
        if(!$this->togglebutton) $this->options['data-widget-togglebutton'] = 'false';
        if(!$this->deletebutton) $this->options['data-widget-deletebutton'] = 'false';
        if(!$this->fullscreenbutton) $this->options['data-widget-fullscreenbutton'] = 'false';
        if(!$this->custombutton) $this->options['data-widget-custombutton'] = 'false';
        if(!$this->collapsed) $this->options['data-widget-collapsed'] = 'false';
        if(!$this->sortable) $this->options['data-widget-sortable'] = 'false';



        $content = '<!-- Widget ID (each widget will need unique ID)--> ';
		$content .= Html::beginTag('div', $this->options);
        $content .= $this->getHeader();
        $content .= Html::beginTag('div');
        $content .= $this->getEditBox();
        $content .= Html::beginTag('div', ['class' => 'widget-body']);
        $content .= ob_get_clean();
        $content .= Html::endTag('div');
        $content .= Html::endTag('div');
        $content .= Html::endTag('div');
        return $content;
    }

    public function getHeader()
    {
        return '<header>' .
                    ($this->icon ? '<span class="widget-icon"> <i class="fa ' . $this->icon . '"></i> </span>' : '' ) .
                    '<h2>' . $this->title . '</h2>				
                </header>';
    }

    public function getEditBox()
    {
        return '<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
										<input class="form-control" type="text">
										<span class="note"><i class="fa fa-check text-success"></i> Change title to update and save instantly!</span>
										
									</div>
									<!-- end widget edit box -->';
    }

}
